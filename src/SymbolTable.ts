/*
MIT License

Copyright (c) 2016, 2017, Mike Lischke

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

// Adapted from the antlr4-c3 project https://github.com/mike-lischke/antlr4-c3

/* eslint-disable eqeqeq */
/* eslint-disable @typescript-eslint/ban-types */
import { Expr } from './parser/Expr';
import { cleanName } from './utils';

export class DuplicateSymbolError extends Error { }

export enum TypeKind {
    Integer,
    Float,
    String,
    Boolean,
    Date,

    Class,
    Array,
    Alias,
}

// The root type interface. Used for typed symbols and type aliases.
export interface Type {
    name: string;

    // The super type of this type or empty if this is a fundamental type.
    // Also used as the target type for type aliases.
    baseTypes: Type[];
    kind: TypeKind;
}

export interface SymbolTableOptions {
    allowDuplicateSymbols?: boolean;
}

// A single class for all fundamental types. They are distinguished via the kind field.
export class FundamentalType implements Type {
    public name: string;

    public get baseTypes(): Type[] { return []; }
    public get kind(): TypeKind { return this.typeKind; }

    public static readonly integerType: FundamentalType = new FundamentalType("int", TypeKind.Integer);
    public static readonly floatType: FundamentalType = new FundamentalType("float", TypeKind.Float);
    public static readonly stringType: FundamentalType = new FundamentalType("string", TypeKind.String);
    public static readonly boolType: FundamentalType = new FundamentalType("bool", TypeKind.Boolean);
    public static readonly dateType: FundamentalType = new FundamentalType("date", TypeKind.Date);

    constructor(name: string, typeKind: TypeKind) {
        this.name = name;
        this.typeKind = typeKind;
    }

    private typeKind: TypeKind;
}

// The root of the symbol table class hierarchy: a symbol can be any manageable entity (like a block), not only
// things like variables or classes.
// We are using a class hierarchy here, instead of an enum or similar, to allow for easy extension and certain
// symbols can so provide additional APIs for simpler access to their sub elements, if needed.
export class Symbol {
    public name = ""; // The name of the scope or empty if anonymous.
    public context: Expr | undefined; // Reference to the parse tree which contains this symbol.

    constructor(name = "") {
        this.name = name;
    }

    /**
     * The parent is usually a scoped symbol as only those can have children, but we allow
     * any symbol here for special scenarios.
     * This is rather an internal method and should rarely be used by external code.
     */
    public setParent(parent: Symbol | undefined) {
        this._parent = parent;
    }

    public get parent(): Symbol | undefined {
        return this._parent;
    }

    public get firstSibling(): Symbol {
        if (this._parent instanceof ScopedSymbol) {
            return this._parent.firstChild!;
        }

        return this;
    }

    /**
     * Returns the symbol before this symbol in its scope.
     */
    public get previousSibling(): Symbol | undefined {
        if (!(this._parent instanceof ScopedSymbol)) {
            return this;
        }

        const result = this._parent.previousSiblingOf(this);
        if (result) {
            return result;
        }
    }

    /**
     * Returns the symbol following this symbol in its scope.
     */
    public get nextSibling(): Symbol | undefined {
        if (!(this._parent instanceof ScopedSymbol)) {
            return this;
        }

        const result = this._parent.nextSiblingOf(this);
        if (result) {
            return result;
        }
    }

    public get lastSibling(): Symbol {
        if (this._parent instanceof ScopedSymbol) {
            return this._parent.lastChild!;
        }

        return this;
    }

    /**
     * Returns the next symbol in definition order, regardless of the scope.
     */
    public get next(): Symbol | undefined {
        if (this.parent instanceof ScopedSymbol) {
            return this.parent.nextOf(this);
        }
    }

    public removeFromParent() {
        if (this._parent instanceof ScopedSymbol) {
            this._parent.removeSymbol(this);
            this._parent = undefined;
        }
    }

    /**
     * Returns the first symbol with a given name, in the order of appearance in this scope
     * or any of the parent scopes (conditionally).
     */
    public resolve(name: string, localOnly = false): Symbol | undefined {
        if (this._parent instanceof ScopedSymbol) {
            return this._parent.resolve(name, localOnly);
        }
    }

    /**
     * Returns the first scope containing the symbol with a given name,
     * starting in this scope and checking all parent scopes.
     */
    public resolveScope(name: string): ScopedSymbol | undefined {
        if (this.parent instanceof ScopedSymbol) {
            return this.parent.resolveScope(name);
        }
    }

    /**
     * Get the outermost entity (below the symbol table) that holds us.
     */
    public get root(): Symbol | undefined {
        let run = this._parent;
        while (run) {
            if (!run._parent || (run._parent instanceof SymbolTable))
                return run;
            run = run._parent;
        }
        return run;
    }

    /**
     * Returns the symbol table we belong too or undefined if we are not yet assigned.
     */
    public get symbolTable(): SymbolTable | undefined {
        if (this instanceof SymbolTable) {
            return this;
        }

        let run = this._parent;
        while (run) {
            if (run instanceof SymbolTable)
                return run;
            run = run._parent;
        }
        return undefined;
    }

    /**
     * Returns the next enclosing parent of the given type.
     */
    public getParentOfType<T extends Symbol>(t: new (...args: any[]) => T): T | undefined {
        let run = this._parent;
        while (run) {
            if (run instanceof t)
                return <T>run;
            run = run._parent;
        }
        return undefined;
    }

    /**
     * The list of symbols from this one up to root.
     */
    public get symbolPath(): Symbol[] {
        const result: Symbol[] = [];
        // eslint-disable-next-line @typescript-eslint/no-this-alias
        let run: Symbol = this;
        while (run) {
            result.push(run);
            if (!run._parent)
                break;
            run = run._parent;
        }
        return result;
    }

    /**
     * Creates a qualified identifier from this symbol and its parent.
     * If `full` is true then all parents are traversed in addition to this instance.
     */
    public qualifiedName(separator = ".", full = false, includeAnonymous = false): string {
        if (!includeAnonymous && this.name.length == 0)
            return "";

        let result: string = this.name.length == 0 ? "<anonymous>" : this.name;
        let run = this._parent;
        while (run) {
            if (includeAnonymous || run.name.length > 0) {
                result = (run.name.length == 0 ? "<anonymous>" : run.name) + separator + result;
                if (!full || !run._parent)
                    break;
            }
            run = run._parent;
        }
        return result;
    }

    protected _parent: Symbol | undefined;
}

// A symbol with an attached type (variables, fields etc.).
export class TypedSymbol extends Symbol {
    public type: Type | undefined;

    constructor(name: string, type?: Type) {
        super(name);
        this.type = type;
    }
}

// A symbol with a scope (so it can have child symbols).
export class ScopedSymbol extends Symbol {
    constructor(name = "") {
        super(name)
    }

    public get children() {
        return this._children;
    }

    public clear() {
        this._children = [];
    }

    /**
     * Adds the given symbol to this scope. If it belongs already to a different scope
     * it is removed from that before adding it here.
     */
    public addSymbol(symbol: Symbol) {
        symbol.removeFromParent();

        // Check for duplicates first.
        const symbolTable = this.symbolTable;
        if (!symbolTable || !symbolTable.options.allowDuplicateSymbols) {
            for (const child of this._children) {
                if (child == symbol || (symbol.name.length > 0 && cleanName(child.name) == cleanName(symbol.name))) {
                    let name = symbol.name;
                    if (name.length == 0)
                        name = "<anonymous>";
                    throw new DuplicateSymbolError("Attempt to add duplicate symbol '" + name + "'");
                }
            }
        }

        this._children.push(symbol);
        symbol.setParent(this);
    }

    public removeSymbol(symbol: Symbol) {
        const index = this._children.indexOf(symbol);
        if (index > -1) {
            this._children.splice(index, 1);
            symbol.setParent(undefined);
        }
    }

    /**
     * Returns all (nested) children of a given type.
     */
    public getNestedSymbolsOfType<T extends Symbol>(t: new (...args: any[]) => T): T[] {
        const result: T[] = [];

        for (const child of this._children) {
            if (child instanceof t)
                result.push(child);
            if (child instanceof ScopedSymbol)
                result.push(...child.getNestedSymbolsOfType(t));
        }

        return result;
    }

    /**
     * Returns symbols from this and all nested scopes in the order they were defined.
     * @param name If given only returns symbols with that name.
     */
    public getAllNestedSymbols(name?: string): Symbol[] {
        const result: Symbol[] = [];

        for (const child of this._children) {
            if (!name || cleanName(child.name) == cleanName(name)) {
                result.push(child);
            }
            if (child instanceof ScopedSymbol)
                result.push(...child.getAllNestedSymbols(name));
        }

        return result;
    }

    /**
     * Returns direct children of a given type.
     */
    public getSymbolsOfType<T extends Symbol>(t: new (...args: any[]) => T): T[] {
        const result: T[] = [];
        for (const child of this._children) {
            if (child instanceof t)
                result.push(<T>child);
        }

        return result;
    }

    /**
     * Returns all symbols of the the given type, accessible from this scope (if localOnly is false),
     * within the owning symbol table.
     */
    public getAllSymbols<T extends Symbol>(t: new (...args: any[]) => T, localOnly = false): Set<Symbol> {
        const result: Set<Symbol> = new Set();

        for (const child of this._children) {
            if (child instanceof t) {
                result.add(child);
            }
        }

        if (!localOnly) {
            if (this._parent instanceof ScopedSymbol) {
                this._parent.getAllSymbols(t, true).forEach(result.add, result);
            }
        }

        return result;
    }

    /**
     * Returns the first symbol with a given name, in the order of appearance in this scope
     * or any of the parent scopes (conditionally).
     */
    public resolve(name: string, localOnly = false): Symbol | undefined {
        for (const child of this._children) {
            if (cleanName(child.name) == cleanName(name))
                return child;
        }

        // Nothing found locally. Let the parent continue.
        if (!localOnly) {
            if (this._parent instanceof ScopedSymbol)
                return this._parent.resolve(name, false);
        }

        return undefined;
    }

    public resolveScope(name: string): ScopedSymbol | undefined {
        if (this instanceof SymbolTable)
            return this

        for (const child of this.children) {
            if (cleanName(child.name) == cleanName(name))
                return this
        }

        if (this.parent instanceof ScopedSymbol)
            return this.parent.resolveScope(name);

        return undefined;
    }

    /**
     * Returns all accessible symbols that have a type assigned.
     */
    public getTypedSymbols(localOnly = true): TypedSymbol[] {
        const result: TypedSymbol[] = []

        for (const child of this._children) {
            if (child instanceof TypedSymbol) {
                result.push(child);
            }
        }

        if (!localOnly) {
            if (this._parent instanceof ScopedSymbol) {
                const localList = (this._parent as ScopedSymbol).getTypedSymbols(true);
                result.push(...localList);
            }
        }

        return result;
    }

    /**
     * The names of all accessible symbols with a type.
     */
    public getTypedSymbolNames(localOnly = true): string[] {
        const result: string[] = [];
        for (const child of this._children) {
            if (child instanceof TypedSymbol) {
                result.push(child.name);
            }
        }

        if (!localOnly) {
            if (this._parent instanceof ScopedSymbol) {
                const localList = (this._parent as ScopedSymbol).getTypedSymbolNames(true);
                result.push(...localList);
            }
        }

        return result;
    }

    /**
     * Returns all direct child symbols with a scope (e.g. classes in a module).
     */
    public get directScopes(): ScopedSymbol[] {
        return this.getSymbolsOfType(ScopedSymbol);
    }

    /**
     * Returns the symbol located at the given path through the symbol hierarchy.
     * @param path The path consisting of symbol names separator by `separator`.
     * @param separator The character to separate path segments.
     */
    public symbolFromPath(path: string, separator = "."): Symbol | undefined {
        const elements = path.split(separator);
        let index = 0;
        if (elements[0] == this.name || elements[0].length == 0)
            ++index;

        // eslint-disable-next-line @typescript-eslint/no-this-alias
        let result: Symbol = this;
        while (index < elements.length) {
            if (!(result instanceof ScopedSymbol)) // Some parts left but found a non-scoped symbol?
                return undefined;

            const child = result._children.find(child => child.name == elements[index]);
            if (!child)
                return undefined;
            result = child;
            ++index;
        }
        return result;
    }

    /**
     * Returns the index of the given child symbol in the child list or -1 if it couldn't be found.
     */
    public indexOfChild(child: Symbol): number {
        return this._children.findIndex((value: Symbol, index: number) => {
            return value == child;
        });
    }

    /**
     * Returns the sibling symbol after the given child symbol, if one exists.
     */
    public nextSiblingOf(child: Symbol): Symbol | undefined {
        const index = this.indexOfChild(child);
        if (index == -1 || index >= this._children.length - 1) {
            return;
        }
        return this._children[index + 1];
    }

    /**
     * Returns the sibling symbol before the given child symbol, if one exists.
     */
    public previousSiblingOf(child: Symbol): Symbol | undefined {
        const index = this.indexOfChild(child);
        if (index < 1) {
            return;
        }
        return this._children[index - 1];
    }

    public get firstChild(): Symbol | undefined {
        if (this._children.length > 0) {
            return this._children[0];
        }
    }

    public get lastChild(): Symbol | undefined {
        if (this._children.length > 0) {
            return this._children[this._children.length - 1];
        }
    }

    /**
     * Returns the next symbol in definition order, regardless of the scope.
     */
    public nextOf(child: Symbol): Symbol | undefined {
        if (!(child.parent instanceof ScopedSymbol)) {
            return;
        }
        if (child.parent != this) {
            return child.parent.nextOf(child);
        }

        if (child instanceof ScopedSymbol && child.children.length > 0) {
            return child.children[0];
        }

        const sibling = this.nextSiblingOf(child);
        if (sibling) {
            return sibling;
        }

        return (this.parent as ScopedSymbol).nextOf(this);
    }

    private _children: Symbol[] = []; // All child symbols in definition order.
}

export class Global extends ScopedSymbol {}
export class Here extends ScopedSymbol {
    public defaultToHere = false;
    public resolveScope(name: string): ScopedSymbol | undefined {
        if (this.defaultToHere) {
            return this;
        }
        // TODO: This could be put in separate function
        for (const child of this.children) {
            if (cleanName(child.name) == cleanName(name))
                return this
        }

        if (this.parent instanceof ScopedSymbol)
        return this.parent.resolveScope(name);
        
        return undefined;
    }
}
export class LocalHere extends Here {
    public defaultToHere = true;
}
export class NamespaceSymbol extends ScopedSymbol {}

export class VariableSymbol extends TypedSymbol {
    constructor(name: string, value: any, type?: Type) {
        super(name, type);
        this.value = value;
    }

    value: any;
}

export class LiteralSymbol extends TypedSymbol {
    constructor(name: string, value: any, type?: Type) {
        super(name, type);
        this.value = value;
    }

    readonly value: any;
}

export class LocalBlock extends ScopedSymbol {
    public defaultLocal = false;
    public resolveScope(name: string): ScopedSymbol | undefined {
        if (this.defaultLocal) {
            return this;
        }
        for (const child of this.children) {
            if (cleanName(child.name) == cleanName(name))
                return this
        }

        if (this.parent instanceof ScopedSymbol)
            return this.parent.resolveScope(name);
        
        return undefined;
    }
}

export class FunctionSymbol extends LocalBlock {
    public defaultLocal = false;

    public getParameters(): ParameterSymbol[] {
        return this.getSymbolsOfType(ParameterSymbol);
    }
}

export class MethodSymbol extends ScopedSymbol {
    public resolveScope(name: string): ScopedSymbol | undefined {
        for (const child of this.children) {
            if (cleanName(child.name) == cleanName(name))
                return this
        }

        // Check if symbol is a member of the parent class
        if (this.parent instanceof ClassSymbol && this.parent.resolveScope(name)) {
            return this.parent;
        }

        // Otherwise it is local to the method
        return this;
    }

    public getParameters(): ParameterSymbol[] {
        return this.getSymbolsOfType(ParameterSymbol);
    }
}

export class ClassSymbol extends ScopedSymbol {
    public baseClasses: ClassSymbol[] = [];

    public resolveScope(name: string): ScopedSymbol | undefined {
        for (const child of this.children) {
            if (cleanName(child.name) == cleanName(name))
                return this
        }

        return undefined;
    }

    public getMethods(includeInherited = false): MethodSymbol[] {
        return this.getSymbolsOfType(MethodSymbol);
    }

    public getFields(includeInherited = false): MemberSymbol[] {
        return this.getSymbolsOfType(MemberSymbol);
    }

    public getFunctions(includeInherited = false): FunctionSymbol[] {
        return this.getSymbolsOfType(FunctionSymbol)
    }
}

export class ParameterSymbol extends VariableSymbol {}
export class MemberSymbol extends VariableSymbol {}
export class UnevaluatedSymbol extends ScopedSymbol{}

// The main class managing all the symbols for a top level entity like a file, library or similar.
export class SymbolTable extends ScopedSymbol {
    constructor(name: string, public readonly options: SymbolTableOptions) {
        super(name);
    }

    public clear() {
        super.clear();
        this.dependencies.clear();
    }

    public addDependencies(...tables: SymbolTable[]) {
        tables.forEach((value, key) => {
            this.dependencies.add(value);
        });
    }

    public removeDependency(table: SymbolTable) {
        if (this.dependencies.has(table)) {
            this.dependencies.delete(table);
        }
    }

    /**
     * Returns instance information, mostly relevant for unit testing.
     */
    public get info() {
        return {
            dependencyCount: this.dependencies.size,
            symbolCount: this.children.length
        };
    }

    public addNewSymbolOfType<T extends Symbol>(t: new (...args: any[]) => T,
        parent: ScopedSymbol | undefined, ...args: any[]): T {

        const result = new t(...args);
        if (!parent || parent == this) {
            this.addSymbol(result);
        } else {
            parent.addSymbol(result);
        }
        return result;
    }

    public getAllSymbols<T extends Symbol>(t?: new (...args: any[]) => T, localOnly = false): Set<Symbol> {
        const type = t ? t : Symbol;
        const result = super.getAllSymbols(type, localOnly);

        if (!localOnly) {
            for (const dependency of this.dependencies) {
                dependency.getAllSymbols(t, localOnly).forEach(result.add, result);
            }
        }

        return result;
    }

    // public symbolWithContext(context: ParseTree): Symbol | undefined {

    //     function findRecursive(symbol: Symbol): Symbol | undefined {
    //         if (symbol.context == context) {
    //             return symbol;
    //         }

    //         if (symbol instanceof ScopedSymbol) {
    //             for (const child of symbol.children) {
    //                 const result = findRecursive(child);
    //                 if (result) {
    //                     return result;
    //                 }
    //             }
    //         }
    //     }

    //     let symbols = this.getAllSymbols(Symbol);
    //     for (const symbol of symbols) {
    //         const result = findRecursive(symbol);
    //         if (result) {
    //             return result;
    //         }
    //     }

    //     for (const dependency of this.dependencies) {
    //         symbols = dependency.getAllSymbols(Symbol);
    //         for (const symbol of symbols) {
    //             const result = findRecursive(symbol);
    //             if (result) {
    //                 return result;
    //             }
    //         }
    //     }
    // }

    // public resolve(name: string, localOnly = false): Symbol | undefined {
    //     let result = super.resolve(name, localOnly);

    //     if (!result && !localOnly) {
    //         for (const dependency of this.dependencies) {
    //             result = dependency.resolve(name, false);
    //             if (result)
    //                 break;
    //         }
    //     }

    //     return result;
    // }

    // Other symbol information available to this instance.
    protected dependencies: Set<SymbolTable> = new Set();
}
