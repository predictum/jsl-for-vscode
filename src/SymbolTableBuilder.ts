/* eslint-disable @typescript-eslint/ban-types */
import { Assign, AssociativeArray, Binary, Call, Expr, ExprVisitor, Glue,
    Grouping, Index, Invalid, List, Literal, LiteralNumeric, LiteralString, Logical,
    Matrix, PostUnary, PreUnary, Variable
} from "./parser/Expr";
import { TokenType } from "./parser/Token";
import {
    Symbol, ScopedSymbol, SymbolTable, VariableSymbol, FunctionSymbol,
    ClassSymbol, LocalBlock, ParameterSymbol, Here, LocalHere, MethodSymbol,
    NamespaceSymbol, UnevaluatedSymbol
} from "./SymbolTable";
import { cleanName, unquote } from "./utils";

export class SymbolTableBuilder implements ExprVisitor<void> {
    private symbolStack: ScopedSymbol[] = [];
    private namespaces: Map<string, NamespaceSymbol> = new Map();
    private isAssigned: Map<Expr, boolean> = new Map();
    private parentAssignmentName: Map<Expr, string[]> = new Map();

    public constructor(private symbolTable: SymbolTable) {
        const here = new Here("here");
        this.symbolTable.addSymbol(here);
        this.symbolStack.push(here);
    }
    
    public build(expr: Expr) {
        return expr.accept(this);
    }

    visitAssignExpr(expr: Assign): void {
        throw new Error("Method not implemented.");
    }
    visitBinaryExpr(expr: Binary): void {
        let scopedNames: string[][] = [];
        let varExpressions: Expr[] = [];
        if (expr.operator.type === TokenType.ASSIGN) {
            if (expr.left instanceof List) {
                [scopedNames, varExpressions] = this.getScopedNamesInList(expr.left);
            } else {
                const scopedName = this.getScopedName(expr.left);
                if (scopedName) {
                    this.parentAssignmentName.set(expr.right, scopedName);
                    scopedNames.push(scopedName);
                    varExpressions.push(expr.left);
                }
            }
        }

        expr.right.accept(this);
        expr.left.accept(this);

        for (let i = 0; i < scopedNames.length; i++) {
            if (this.isAssigned.get(expr.right)) return;
            const resolved = this.resolveSymbolScope(scopedNames[i]);
            if(resolved) {
                this.addNewSymbol(VariableSymbol, resolved.scope, varExpressions[i], resolved.name, undefined);
            }
        }
    }
    visitCallExpr(expr: Call): void {
        expr.callee.accept(this);

        const scopedName = this.getScopedName(expr.callee);
        if(!scopedName)
            return;

        // Only switch on builtin unscoped functions
        const callName = scopedName.length === 1 ? scopedName[0] : "";
        switch(cleanName(callName)) {
            // Functions relevant to scoping
            case "FUNCTION": {
                if (this.isAssigned.get(expr))
                    break; // Already assigned in NewCustomFunction 
                let name = "<Function>";
                let resolved: {scope: ScopedSymbol | undefined, name: string} | undefined;
                const scopedName = this.parentAssignmentName.get(expr);
                if (scopedName) {
                    name = scopedName[scopedName.length-1];
                    resolved = this.resolveSymbolScope(scopedName);
                }
                let parent = this.currentSymbol();
                if (resolved) {
                    parent = resolved.scope;
                    name = resolved.name;
                }
                this.isAssigned.set(expr, true);
                this.pushNewSymbol(FunctionSymbol, parent, expr, name);
                // Get parameters and locals
                this.addLocalsFromArgList(expr.args, 0, ParameterSymbol);
                if (expr.args.length === 3) {
                    this.addLocalsFromArgList(expr.args, 1, VariableSymbol);
                }
                break;
            }
            case "NEWCUSTOMFUNCTION": {
                const args = expr.args;
                if (args && args.length >= 3) {
                    // Checks that arguments are valid
                    let namespace: string;
                    let functionName: string;
                    if (args[0] instanceof LiteralString && typeof args[0].value === 'string') {
                        namespace = unquote(args[0].value);
                    } else if (args[0] instanceof Variable) {
                        namespace = unquote(args[0].name.lexeme);
                    } else {break}
                    if (args[1] instanceof LiteralString && typeof args[1].value === 'string') {
                        functionName = unquote(args[1].value);
                    } else if (args[1] instanceof Variable) {
                        functionName = unquote(args[1].name.lexeme);
                    } else {break}
                    if (!(args[2] instanceof Call)) break;
                    const functionExpr = args[2];
                    const arg3name = this.getScopedName(functionExpr.callee);
                    if (!arg3name || arg3name.length > 1 || cleanName(arg3name[0]) !== "FUNCTION") break;

                    // Add FunctionSymbol to the namespace
                    let namespaceSymbol = this.namespaces.get(namespace);
                    if (!namespaceSymbol) {
                        namespaceSymbol = new NamespaceSymbol(namespace);
                        this.namespaces.set(namespace, namespaceSymbol);
                        this.symbolTable.addSymbol(namespaceSymbol);
                    }
                    this.isAssigned.set(functionExpr, true);
                    this.pushNewSymbol(FunctionSymbol, namespaceSymbol, functionExpr, functionName);
                    // Get parameters and locals
                    this.addLocalsFromArgList(functionExpr.args, 0, ParameterSymbol)
                    if (functionExpr.args.length === 3) {
                        this.addLocalsFromArgList(functionExpr.args, 1, VariableSymbol)
                    }
                }
                break;
            }
            case "DEFINECLASS": {
                let name: string | undefined = "<Class>";
                if (expr.args.length > 0 && expr.args[0] instanceof LiteralString && typeof expr.args[0].value === 'string') {
                    name = expr.args[0].value
                    name = cleanName(unquote(name));
                }
                this.pushNewSymbol(ClassSymbol, this.symbolTable, expr, name);
                break;
            }
            case "METHOD": {
                let name = "<Method>";
                let resolved: {scope: ScopedSymbol | undefined, name: string} | undefined;
                const scopedName = this.parentAssignmentName.get(expr);
                if (scopedName) {
                    name = scopedName[scopedName.length-1];
                    resolved = this.resolveSymbolScope(scopedName);
                }
                let parent = this.currentSymbol();
                if (resolved) {
                    parent = resolved.scope;
                    name = resolved.name;
                }
                this.isAssigned.set(expr, true);
                this.pushNewSymbol(MethodSymbol, parent, expr, name);
                // Get parameters
                this.addLocalsFromArgList(expr.args, 0, ParameterSymbol)
                break;
            }
            case "LOCAL": {
                this.pushNewSymbol(LocalBlock, this.currentSymbol(), expr, "<Local>");
                // Get parameters
                this.addLocalsFromArgList(expr.args, 0, VariableSymbol)
                break;
            }
            case "LOCALHERE": {
                this.pushNewSymbol(LocalHere, this.currentSymbol(), expr, "<LocalHere>");
                break;
            }
            case "NAMESDEFAULTTOHERE": {
                // Only allow in Global/Here scope
                if (this.symbolStack.length > 1)
                    break; 
                const args = expr.args;
                if (!args || args.length > 1) 
                    break; // JSLError
                if (args[0] instanceof LiteralNumeric) {
                    const val = args[0].value;
                    // TODO: cleaner way to get topmost Here.
                    // Especially important with more than one document
                    const here = this.symbolStack[0] as Here;
                    if (val === 0) {
                        here.defaultToHere = false;
                    } else {
                        here.defaultToHere = true;
                    }
                }
                break;
            }
            // Unevaluated
            case "NAMEEXPR":
            case "JSLQUOTE": {
                // The arguments of the function will not be visited
                return;
            }
            default: break;
        }

        // Traverse function arguments
        for (const e of expr.args) {
            e.accept(this);
        }

        // Pop the scope
        switch(cleanName(callName)) {
            // Scopes
            case "FUNCTION":
            case "DEFINECLASS":
            case "METHOD":
            case "LOCAL":
            case "LOCALHERE": {
                this.popSymbol();
                break;
            }
            default: break;
        }
    }
    visitListExpr(expr: List): void {
        for (const e of expr.contents) {
            e.accept(this);
        }
    }
    visitPreUnaryExpr(expr: PreUnary): void {
        expr.expression.accept(this);
    }

    // Just keep visiting subexpressions
    visitGlueExpr(expr: Glue): void {
        for (const e of expr.expressions) {
            e.accept(this);
        }
    }
    visitGroupingExpr(expr: Grouping): void {
        expr.expression.accept(this);
    }
    visitIndexExpr(expr: Index): void {
        expr.expression.accept(this);
        for (const e of expr.indices) {
            e.accept(this);
        }
    }
    visitLogicalExpr(expr: Logical): void {
        expr.left.accept(this);
        expr.right.accept(this);
    }
    visitPostUnaryExpr(expr: PostUnary): void {
        expr.expression.accept(this);
    }

    // Terminals
    visitAssociativeArrayExpr(expr: AssociativeArray): void {
        return;
    }
    visitLiteralExpr(expr: Literal): void {
        return;
    }
    visitMatrixExpr(expr: Matrix): void {
        return;
    }
    visitVariableExpr(expr: Variable): void {
        return;
    }
    visitInvalidExpr(expr: Invalid): void {
        return;
    }

    /**
     * Adds a new symbol to the current symbol TOS.
     *
     * @param type The type of the symbol to add.
     * @param parent The parent to add the symbol to.
     * @param context The symbol's parse tree, to allow locating it.
     * @param args The actual arguments for the new symbol.
     *
     * @returns The new symbol.
     */
    private addNewSymbol<T extends Symbol>(type: new (...args: any[]) => T,
        parent: ScopedSymbol | undefined, context: Expr,
        ...args: any[]): T {
        const symbol = this.symbolTable.addNewSymbolOfType(type, parent, ...args);
        symbol.context = context;

        return symbol;
    }

    /**
     * Creates a new symbol and starts a new scope with it on the symbol stack.
     *
     * @param type The type of the symbol to add.
     * @param expr The symbol's expression, to allow locating it.
     * @param args The actual arguments for the new symbol.
     *
     * @returns The new scoped symbol.
     */
    private pushNewSymbol<T extends ScopedSymbol>(type: new (...args: any[]) => T,
        parent: ScopedSymbol | undefined, expr: Expr,
        ...args: any[]): Symbol {
        // Scope has to be resolved as local/here/global
        const symbol = this.symbolTable.addNewSymbolOfType<T>(type, parent, ...args);
        symbol.context = expr;
        this.symbolStack.push(symbol);

        return symbol;
    }

    private popSymbol(): Symbol | undefined {
        return this.symbolStack.pop();
    }

    private currentSymbol<T extends ScopedSymbol>(): T | Here | SymbolTable | undefined {
        // TODO: There's a better place for this. Maybe part of resolve. 
        // Also Here shouldn't need to be on the stack
        if (this.symbolStack.length === 1) {
            const here = this.symbolStack[0] as Here;
            if (here.defaultToHere) {
                return here;
            } else {
                return this.symbolTable;
            }
        } else if (this.symbolStack.length === 0) {
            return undefined;
        }

        return this.symbolStack[this.symbolStack.length - 1] as T;
    }

    private addVariablesInList<T extends VariableSymbol>(variables: Expr[], t: new (...args: any[]) => T) {
        for(const v of variables) {
            let arg = v;
            if (v instanceof Binary && v.operator.type === TokenType.ASSIGN) {
                this.isAssigned.set(v.right, true);
                arg = v.left;
            }
            const scopedName = this.getScopedName(arg);
            if(scopedName){
                const varName = scopedName[scopedName.length-1];
                if(t === VariableSymbol && cleanName(varName) === "DEFAULTLOCAL") {
                    const current = this.currentSymbol() as LocalBlock;
                    current.defaultLocal = true;
                } else {
                    this.addNewSymbol(t, this.currentSymbol(), arg, varName, undefined)
                }
            }
        }
    }

    private addLocalsFromArgList<T extends VariableSymbol>(args: Expr[], argNumber: number,
        variableType: new (...args: any[]) => T) {
        const arg = args[argNumber];
        if (arg instanceof List) {
            const locals = arg.contents;
            this.addVariablesInList(locals, variableType);
        }
    }

    /**
     * Resolves the scope that a scoped name belongs to
     *
     * @param scopedName A list of namespaces and names of a variable
     *
     * @returns The scope the name belongs to
     */
     private resolveSymbolScope(scopedName: string[]) {
        // Get scope type
        let scope = this.currentSymbol();
        if (!scope)
            scope = this.symbolTable;

        let symbolName = scopedName[scopedName.length - 1];
        let resolvedScope: ScopedSymbol | undefined;
        if (scope instanceof UnevaluatedSymbol) {
            // Do not resolve
            resolvedScope = scope;
        } else if (scope instanceof ClassSymbol) {
            symbolName = scopedName.join(":"); // No scoped names allowed
            resolvedScope = scope;
        } else if (scopedName.length === 1) {
            resolvedScope = scope.resolveScope(symbolName);
        } else if (scopedName.length === 2) {
            // Explicitly scoped names
            switch(scopedName[0]) {
                case "GLOBAL": {
                    resolvedScope = this.symbolTable;
                    break;
                }
                case "HERE": {
                    // Add to the closest Here scope
                    for (let i = this.symbolStack.length - 1; i >= 0; i--) {
                        const symbol = this.symbolStack[i];
                        if (symbol instanceof Here) {
                            resolvedScope = symbol;
                            break;
                        }
                    }
                    break;
                }
                // Treating builtin JMP scopes as Local
                case "WINDOW": 
                case "BOX": 
                case "PLATFORM":
                case "LOCAL": {
                    // Add to current scope
                    resolvedScope = scope;
                    break;
                }
                case "BUILTIN": {
                    return;
                }
                // TODO: "ROOT"
                default: {
                    // TODO: NewObjects. Right now an object's name is assumed to be a namespace.
                    // Namespaces
                    const namespace = scopedName[0];
                    let namespaceSymbol = this.namespaces.get(namespace);
                    if (!namespaceSymbol) {
                        namespaceSymbol = new NamespaceSymbol(namespace);
                        this.namespaces.set(namespace, namespaceSymbol);
                        this.symbolTable.addSymbol(namespaceSymbol);
                    }
                    resolvedScope = namespaceSymbol;
                    break;
                }
            }
        }
        // TODO: Handle more than one scope in the path e.g. here:className:member
        return {scope: resolvedScope, name: symbolName};
    }

    // Returns the scoped name of an expression if it is a name. Returns undefined otherwise.
    private getScopedName(expr: Expr): string[] | undefined {
        if (expr instanceof Variable) {
            return [expr.name.lexeme];
        }
        if (expr instanceof PreUnary && [TokenType.COLON, TokenType.DOUBLE_COLON, TokenType.TRIPLE_COLON].includes(expr.operator.type) && expr.expression instanceof Variable) {
            const scopedName = [expr.expression.name.lexeme];
            switch (expr.operator.type) {
                case TokenType.COLON: {
                    scopedName.unshift("DATATABLE");
                    break;
                }
                case TokenType.DOUBLE_COLON: {
                    scopedName.unshift("GLOBAL");
                    break;
                }
                case TokenType.TRIPLE_COLON: {
                    scopedName.unshift("ROOT");
                    break;
                }
            }
            return scopedName
        }
        if (expr instanceof Binary && expr.operator.type === TokenType.COLON && expr.right instanceof Variable) {
            const scopedName = [];
            if (expr.left instanceof Variable) {
                scopedName.push(expr.left.name.lexeme);
            } else if (expr.left instanceof LiteralString && typeof expr.left.value === 'string') {
                // Namespace can be string
                scopedName.push(expr.left.value)
            } // TODO: Handle Namespace("name") as left
            scopedName.push(expr.right.name.lexeme);
            return scopedName;
        }
        return undefined;
    }

    // Gets a list of scoped names and the variable Exprs they belong to
    private getScopedNamesInList(list: List): [string[][], Expr[]] {
        const scopedNames: string[][] = [];
        const varExpressions: Expr[] = [];
        for (const expr of list.contents) {
            const name = this.getScopedName(expr);
            if (name) {
                scopedNames.push(name);
                varExpressions.push(expr);
            }
        }
        return [scopedNames, varExpressions];
    }
}
