import { CancellationToken, Hover, HoverProvider, MarkdownString, Position, TextDocument } from "vscode";
import { JSLFunction } from "../types";
import { cleanName, concat_descriptions } from "../utils";
import { ProviderBackend } from "./ProviderBackend";
import { runJMPCommand } from "../jsl_socket";

export class JSLHoverProvider implements HoverProvider {
    constructor(private backend: ProviderBackend) {}

    public provideHover(document: TextDocument, position: Position, token: CancellationToken):
        Promise<Hover> {

        return new Promise<Hover>((resolve, reject) => {
            // helper regexes
            const valid_var_chars = /[A-Za-z_][\w\s'#$%.?\\]*/;
            const scoped_var = new RegExp("(?:"+valid_var_chars.source + ":" + ")?" + valid_var_chars.source); // this doesn't deal with :: global scope shorthand
            // Look for message
            let range = document.getWordRangeAtPosition(position, new RegExp(/<<\s*/.source + valid_var_chars.source));
            let word = document.getText(range);
            let hover_type="message";
            if (!range) {
                // Look for function call
                range = document.getWordRangeAtPosition(position, new RegExp(scoped_var.source + /\(/.source));
                word = document.getText(range).slice(0, -1).trim();
                hover_type = "function"
            }
            if (!range) {
                // Look for a variable
                range = document.getWordRangeAtPosition(position, scoped_var);
                word = document.getText(range).trim()
                hover_type = "variable"
            }
            if (!range)
                return reject();
            const cleanedName = cleanName(word, String.prototype.toLowerCase);

            if (hover_type === "message") {
                const messageName = cleanedName.substring(2).trim();
                const message = this.backend.jslMessages[messageName];
                return resolve(this.getHover(message));
            } else if (hover_type === "function") {
                if(cleanedName.includes(':')) {
                    // Scoped Functions
                    const [scope, func] = cleanedName.split(':');
                    if (Object.prototype.hasOwnProperty.call(this.backend.jslFunctions, scope)) {
                        const obj = this.backend.jslFunctions[scope]['functions'][func];
                        return resolve(this.getHover(obj));
                    }
                } else {
                    // Check globals and builtins
                    const builtinFunc = this.backend.jslFunctions.builtin.functions[cleanedName];
                    if (builtinFunc) {
                        return resolve(this.getHover(builtinFunc));
                    }
                    if (Object.prototype.hasOwnProperty.call(this.backend.jslFunctions, 'global')) {
                        const globalFunc = this.backend.jslFunctions.global.functions[cleanedName];
                        return resolve(this.getHover(globalFunc));
                    }
                    
                    return undefined;
                }
            } else if (hover_type === "variable") {
                runJMPCommand("nameExpr("+cleanedName+")", true, false).then((result)=>{ // this won't start jmp.  change to runJMPCommand(cleanedName, false, false) to see the errors
                        const mdString = new MarkdownString();
                    mdString.appendText("Variable: "+word+"\nValue: "+result)
                    resolve(new Hover(mdString))
                }).catch(reject)
            }
            else {
                return undefined
            }
        })
    }

    getHover(func: JSLFunction): Hover {
        const mdString = concat_descriptions(func.prototypes, func.descriptions);
        return new Hover(mdString)
    }
}
