/* eslint-disable @typescript-eslint/ban-types */
import {
    CancellationToken, DocumentSymbol, DocumentSymbolProvider, TextDocument, Range, SymbolKind
} from "vscode";
import { Symbol, ScopedSymbol, UnevaluatedSymbol } from "../SymbolTable";
import { ProviderBackend } from "./ProviderBackend";

export class JSLDocumentSymbolProvider implements DocumentSymbolProvider {
    constructor(private backend: ProviderBackend) {}

    public provideDocumentSymbols(document: TextDocument,
            token: CancellationToken): Thenable<DocumentSymbol[]> {
        return new Promise<DocumentSymbol[]>((resolve, _reject) => {
            // TODO: Leaving this extra parse in for now otherwise the
            // provided symbols are always one parse behind
            this.backend.parse(document.getText()).then(() => {
                const documentSymbols: DocumentSymbol[] = [];
                // Transform symbolTable info to DocumentSymbols
                const symbols = this.backend.symbolTable.getAllSymbols(Symbol, true);
                for (const symbol of symbols) {
                    // Don't add Here as a DocumentSymbol
                    if (symbol.constructor.name === "Here") {
                        const here = symbol as ScopedSymbol;
                        const hereSymbols = here.getAllSymbols(Symbol, true);
                        for (const symbol of hereSymbols) {
                            const childSymbols = getDocumentSymbols(symbol);
                            if (childSymbols) documentSymbols.push(childSymbols)
                        }
                    } else {
                        const childSymbols = getDocumentSymbols(symbol);
                        if (childSymbols) documentSymbols.push(childSymbols)
                    }
                }
                resolve(documentSymbols);
            })
        });
    }
}

// recursive calling of get doc symbols
function getDocumentSymbols<T extends Symbol>(symbol: T) {
    const kind = translateSymbolKind(symbol);
    if (symbol instanceof UnevaluatedSymbol) {
        // Skip all of its children
        return;
    } else if (symbol instanceof ScopedSymbol) {
        const parent = symbol.parent ? symbol.parent.name : "";
        const range = getRange(symbol);
        const documentSymbol = new DocumentSymbol(symbol.name, parent, kind, range, range);
        const symbols = symbol.getAllSymbols(Symbol, true);
        for (const s of symbols) {
            const childSymbols = getDocumentSymbols(s);
            if (childSymbols) documentSymbol.children.push(childSymbols);
        }
        return documentSymbol;
    } else {
        const range = getRange(symbol);
        return new DocumentSymbol(symbol.name, symbol.parent!.name, kind, range, range)
    }
}

function translateSymbolKind<T extends Symbol>(symbol: T) {
    switch(symbol.constructor.name) {
        case "VariableSymbol": {
            return SymbolKind.Variable
        }
        case "FunctionSymbol": {
            return SymbolKind.Function
        }
        case "ParameterSymbol": {
            return SymbolKind.Variable
        }
        case "ClassSymbol": {
            return SymbolKind.Class
        }
        case "MethodSymbol": {
            return SymbolKind.Method
        }
        case "NamespaceSymbol": {
            return SymbolKind.Namespace
        }
        default: return SymbolKind.Null
    }
}

function getRange(symbol: Symbol): Range {
    const ctx = symbol.context;
    if(!ctx) {
        // Namespaces
        // Whole range of document so nested symbols display better in the top bar
        // NOTE: This does mean multiple namespaces will not display well together
        return new Range(0, 0, Number.MAX_SAFE_INTEGER, Number.MAX_SAFE_INTEGER);
    }

    return new Range(
        ctx.range.startLine - 1, ctx.range.startCol - 1,
        ctx.range.endLine - 1, ctx.range.endCol - 1
    );
}
