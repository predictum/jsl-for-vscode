import {CancellationToken, DocumentFormattingEditProvider, FormattingOptions, Range, TextDocument, TextEdit, window} from 'vscode';
import {runJMPCommand} from '../jsl_socket';
import tmp from 'tmp';
import fs from 'fs';

export class JSLFormattingProvider implements DocumentFormattingEditProvider {

    async provideDocumentFormattingEdits(document: TextDocument, options: FormattingOptions, token: CancellationToken): Promise<TextEdit[]> {

        return new Promise<TextEdit[]> ((resolve, reject) => {
            // Save the text to a temp file so JMP can load directly from the file
            // This avoids having to deal with escaped characters
            tmp.file({ postfix: '.jsl' }, (err, path) => {
                if (err) return reject(err.message);
                fs.writeFile(path, document.getText(), err => {
                    if (err) return reject(err.message);

                    const jslFormatScript = `
                    _VSCODE_SCRIPT_BOX = Script Box(loadTextFile("${path}"));
                    _VSCODE_SCRIPT_BOX << setWidth(1000);
                    _VSCODE_SCRIPT_BOX << Reformat;
                    try(
                        _VSCODE_JSL = _VSCODE_SCRIPT_BOX << getText;
                        parse(_VSCODE_JSL); // Only valid jsl should be reformatted
                        saveTextFile("${path}", _VSCODE_JSL) // write to the temp file
                    ,
                        write("SYNTAX ERROR")
                    );`;
                    const firstLine = document.lineAt(0);
                    const lastLine = document.lineAt(document.lineCount - 1);
                    const fullRange = new Range(firstLine.range.start, lastLine.range.end);
                    runJMPCommand(jslFormatScript, true).then((jmplog) => {
                        if (jmplog.endsWith("SYNTAX ERROR")) {
                            window.showErrorMessage("Invalid syntax found, could not format the document.");
                            reject("Invalid syntax found, could not format the document.");
                        } else {
                            fs.readFile(path, (err, result) => {
                                const formattedJSL = result.toString();
                                resolve([TextEdit.replace(fullRange, formattedJSL)]);
                            })
                        }
                    })
                })
            });
        })
    }
}
