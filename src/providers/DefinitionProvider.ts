/* eslint-disable @typescript-eslint/ban-types */
import { CancellationToken, DefinitionProvider, Location, Position, Range, TextDocument } from "vscode";
import { Expr } from "../parser/Expr";
import { Symbol, ScopedSymbol, Here, UnevaluatedSymbol, NamespaceSymbol } from "../SymbolTable";
import { cleanName, positionInScope } from "../utils";
import { ProviderBackend } from "./ProviderBackend";

export class JSLDefinitionProvider implements DefinitionProvider {
    constructor(private backend: ProviderBackend) {}

    private getClosestScope(position: Position) {
        const _getClosestScope = (scope: ScopedSymbol): ScopedSymbol => {
            for (const s of scope.getSymbolsOfType(ScopedSymbol)) {
                if (Object.prototype.hasOwnProperty.call(s, 'context') && !(s instanceof UnevaluatedSymbol) && positionInScope(position, s)) {
                    return _getClosestScope(s);
                }
            }
            return scope
        }

        let closestScope: ScopedSymbol;
        for (const topScope of this.backend.symbolTable.getSymbolsOfType(ScopedSymbol)) {
            // Search each top level scope. If the returned scope is the top scope itself, keep searching.
            closestScope = _getClosestScope(topScope);
            if (closestScope !== topScope)
                return closestScope
        }

        // Check scopes nested in Global
        closestScope = _getClosestScope(this.backend.symbolTable);
        if (closestScope !== this.backend.symbolTable)
            return closestScope;

        // Otherwise closest scope is Here
        const hereScope = this.backend.symbolTable.getSymbolsOfType(Here)[0];
        return hereScope;
    }

    public provideDefinition(document: TextDocument, position: Position, token: CancellationToken): Promise<Location> {
        return new Promise<Location>((resolve, reject) => {
            // TODO: get name from symbol tree given the position.
            // TODO: If the position was in the scope name, go to the Namespace/Class definition
            const wordRange = document.getWordRangeAtPosition(position, /(?:[A-Za-z_][\w\s'#$%.?\\]*:)?\s*[A-Za-z_][\w\s'#$%.?\\]*/);
            const maybeScopedName = document.getText(wordRange);
            let scopeName: string, name: string;
            let closestScope: ScopedSymbol;
            if (maybeScopedName.includes(':')) {
                // Explicitly scoped name
                [scopeName, name] = maybeScopedName.split(':');
                switch (cleanName(scopeName)) {
                    case "HERE":
                        closestScope = this.backend.symbolTable.getSymbolsOfType(Here)[0];
                        break;
                    default:
                        {
                            // TODO: Classes. Needs https://gitlab.com/predictum/jsl-for-vscode/-/issues/58 first.
                            const namespaces = this.backend.symbolTable.getAllSymbols(NamespaceSymbol, true);
                            for (const ns of namespaces) {
                                if (ns instanceof NamespaceSymbol && cleanName(ns.name) === cleanName(scopeName)) {
                                    closestScope = ns;
                                    break;
                                }
                            }
                            return reject(`Could not find a namespace called ${scopeName}`);
                        }
                }
            } else {
                name = maybeScopedName;
                closestScope = this.getClosestScope(position);
            }

            const definitionScope = closestScope.resolve(name)?.parent as ScopedSymbol;
            if (!definitionScope)
                return reject();

            let definition: Symbol | undefined;
            for (const child of definitionScope.getAllNestedSymbols().reverse()) {
                if (cleanName(child.name) === cleanName(name) && child.context) {
                    if (closestScope === definitionScope && isAfterPosition(child.context, position)) {
                        // Definition must be before first use in the same scope
                        continue
                    }
                    definition = child
                }
            }
            if (definition?.context) {
                const ctx = definition.context;
                const range = new Range(
                    ctx.range.startLine - 1, ctx.range.startCol-1,
                    ctx.range.endLine - 1, ctx.range.endCol-1
                );
                return resolve(new Location(document.uri, range));
            }
        })
    }
}

// Check if the context is after the position in the same scope
function isAfterPosition(expr: Expr, position: Position) {
    const ctxStart = new Position(expr.range.startLine-1, expr.range.startCol-1);
    return ctxStart.isAfter(position)
}
