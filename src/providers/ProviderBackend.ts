import { JSLFunctions, JSLScopedFunctions } from '../types';
import { waitForFileCreation } from '../utils';
import { _parse } from '../Parse';
import { SymbolTable } from '../SymbolTable';
import { SymbolTableBuilder } from '../SymbolTableBuilder';
import { Diagnostic } from 'vscode';

import * as path from 'path';
import * as fs from 'fs';
import { Linter } from './Linter';

// An instance of this class will be shared among providers
// to reuse parse results and updates to functions from 
// the scripting index
export class ProviderBackend {
    private _jslFunctions;
    private _jslMessages;
    private _symbolTable;
    constructor(_jslFunctions: JSLScopedFunctions, _jslMessages: JSLFunctions) {
        this._jslFunctions = _jslFunctions;
        this._jslMessages = _jslMessages;
        this._symbolTable = new SymbolTable("global", {allowDuplicateSymbols: true});
    }

    get jslFunctions(): JSLScopedFunctions {
        return this._jslFunctions;
    }

    set jslFunctions(latestFunctions: JSLScopedFunctions) {
        this._jslFunctions = latestFunctions;
    }

    get jslMessages(): JSLFunctions {
        return this._jslMessages;
    }

    get symbolTable(): SymbolTable {
        return this._symbolTable;
    }

    set symbolTable(latestFunctions: SymbolTable) {
        this._symbolTable = latestFunctions;
    }

    // Updates all providers with new functions from the Scripting Index
    async updateProviders(): Promise<void> {
        const functionsJSON = path.resolve(__dirname, "../../data/JSLFunctions_User.json");

        await waitForFileCreation(functionsJSON, 5000);
        let raw = fs.readFileSync(functionsJSON, "utf-8");
        // Remove BOM character
        if (raw.charCodeAt(0) === 0xFEFF) {
            raw = raw.substring(1);
        }
        this.jslFunctions = JSON.parse(raw);
    }

    async parse(text: string, diagnostics?: Diagnostic[]): Promise<void> {
        _parse(text, diagnostics).
        then(tree => {
            this._symbolTable = new SymbolTable("global", {allowDuplicateSymbols: true});
            const builder = new SymbolTableBuilder(this.symbolTable);
            builder.build(tree);
            if (diagnostics) {
                const linter = new Linter(this._symbolTable, diagnostics);
                linter.lint(tree);
            }
        });
    }
}
