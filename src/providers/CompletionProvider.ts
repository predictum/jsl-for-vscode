/* eslint-disable @typescript-eslint/ban-types */
import {
    CompletionItemProvider, CompletionContext, CompletionItemKind,
    CompletionItem, TextDocument, Position, CancellationToken, MarkdownString
} from "vscode";
import { Here, LocalBlock, Symbol, ScopedSymbol, SymbolTable, UnevaluatedSymbol, FunctionSymbol } from "../SymbolTable";
import { JSLFunction } from "../types";
import { concat_descriptions, formatName, positionInScope } from "../utils";
import { ProviderBackend } from "./ProviderBackend";

// TODO: Only do the work to build builtins completions once
// TODO: Get rid of duplicates from Local + buitlin completions
abstract class AbstractJSLCompletionProvider implements CompletionItemProvider {
    constructor(protected backend: ProviderBackend) {}

    public provideCompletionItems(document: TextDocument, position: Position, token: CancellationToken, context: CompletionContext): CompletionItem[] {
        throw new Error("Method not implemented.");
    }

    protected addCompletionsForScope(scope: string, completions: CompletionItem[]) {
        if (Object.prototype.hasOwnProperty.call(this.backend.jslFunctions, scope)) {
            const unformatted_scope = this.backend.jslFunctions[scope].unformatted_name;
            Object.values(this.backend.jslFunctions[scope]['functions']).forEach((func: JSLFunction) => {
                const funcName = formatName(func.unformatted_name);
                const completion = new CompletionItem(funcName, CompletionItemKind.Function);
                // Allow for spaces while typing the name
                completion.filterText = func.unformatted_name.split('').join(' ');
                completion.documentation = func.descriptions[0];
                completion.detail = unformatted_scope;
                completions.push(completion)
            });
        }
    }
}

// Provides completions triggered by ':'
export class ScopedNameCompletionProvider extends AbstractJSLCompletionProvider {
    public provideCompletionItems(document: TextDocument, position: Position, token: CancellationToken, context: CompletionContext): CompletionItem[] {
        const completions: CompletionItem[] = [];
        // Get full JSL identifier at position
        const scopeRange = document.getWordRangeAtPosition(position, /[A-Za-z_][\w\s'#$%.?\\]*:(?:\s*[A-Za-z_][\w\s'#$%.?\\]*)?/);
        if(scopeRange) {
            const scope = document.getText(scopeRange).split(':')[0].toLowerCase().replace(/\s+/g, '');

            this.addCompletionsForScope(scope, completions);
        }
        return completions;
    }
}

// Provides untriggered completions
export class JSLCompletionProvider extends AbstractJSLCompletionProvider {

    public provideCompletionItems(document: TextDocument, position: Position, token: CancellationToken, context: CompletionContext): CompletionItem[] {
        const completions: CompletionItem[] = [];
        // Get full JSL identifier at position
        const wordRange = document.getWordRangeAtPosition(position, /[A-Za-z_][\w\s'#$%.?\\]*/);
        // Only continue if not preceded by a semicolon
        if (wordRange) {
            const prefix = document.lineAt(position).text.substr(0, wordRange.start.character).trim();
            if (prefix.endsWith(':') || prefix.endsWith('<<'))
                return completions
        }
        // Add namespaces
        for (const scope of Object.keys(this.backend.jslFunctions)) {
            const unformatted_scope = this.backend.jslFunctions[scope]['unformatted_name'];
            const completion = new CompletionItem(unformatted_scope, CompletionItemKind.Module);
            completion.filterText = unformatted_scope.split('').join(' ');
            completion.commitCharacters = [':'];
            completions.push(completion);
        }
        // Add builtin and global functions
        this.addCompletionsForScope('builtin', completions);
        this.addCompletionsForScope('global', completions); // TODO: Should insert :: if names default to here(1)
        return completions;
    }
}

// Provides completions triggered by '<<'
export class MessageCompletionProvider extends AbstractJSLCompletionProvider {

    public provideCompletionItems(document: TextDocument, position: Position, token: CancellationToken, context: CompletionContext): CompletionItem[] {
        const completions: CompletionItem[] = [];
        const messageRange = document.getWordRangeAtPosition(position, /<<(?:\s*[A-Za-z_][\w\s'#$%.?\\]*)?/);
        if(messageRange) {
            Object.values(this.backend.jslMessages).forEach((message: JSLFunction) => {
                const messageName = formatName(message.unformatted_name);
                const completion = new CompletionItem(messageName, CompletionItemKind.Method);
                // Allow for spaces while typing the name
                completion.filterText = message.unformatted_name.split('').join(' ');
                completion.documentation = concat_descriptions(message.prototypes, message.descriptions);
                completions.push(completion)
            });
        }
        return completions;
    }
}

export class LocalCompletionProvider extends AbstractJSLCompletionProvider {

    getLocalCompletions(position: Position, symbolTable: SymbolTable): CompletionItem[] {
        const stack: ScopedSymbol[] = [];
        const completions: CompletionItem[] = [];
        const seen = new Set<string>();
        stack.push(symbolTable);

        // TODO: With duplicate symbols this gets the symbol in the most outer scope
        // when it should use the most specific scope. Only matters for symbol type shown.
        while (stack.length) {
            const s = stack.pop();
            if (!Object.prototype.hasOwnProperty.call(s, 'context') || (s && positionInScope(position, s))) {
                for (const child of s!.children) {
                    if (child instanceof Here || 
                          (child instanceof LocalBlock && !(child instanceof FunctionSymbol)) || child instanceof UnevaluatedSymbol)
                        continue
                    if (!(seen.has(child.name))) {
                        const completion = new CompletionItem(child.name, translateSymbolKind(child))
                        completions.push(completion)
                        seen.add(child.name);
                    }
                }
            }
            for (const scope of s!.getSymbolsOfType(ScopedSymbol)) {
                stack.push(scope)
            }
        }
        return completions
    }

    public provideCompletionItems(document: TextDocument, position: Position, token: CancellationToken, context: CompletionContext): CompletionItem[] {
        return this.getLocalCompletions(position, this.backend.symbolTable);
    }

}

function translateSymbolKind<T extends Symbol>(symbol: T) {
    switch(symbol.constructor.name) {
        case "VariableSymbol": {
            return CompletionItemKind.Variable
        }
        case "FunctionSymbol": {
            return CompletionItemKind.Function
        }
        case "ParameterSymbol": {
            return CompletionItemKind.Variable
        }
        case "ClassSymbol": {
            return CompletionItemKind.Class
        }
        case "MethodSymbol": {
            return CompletionItemKind.Method
        }
        case "NamespaceSymbol": {
            return CompletionItemKind.Module
        }
        default: return
    }
}

