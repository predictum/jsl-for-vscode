/* eslint-disable @typescript-eslint/ban-types */
import { Diagnostic, DiagnosticSeverity } from "vscode";
import { Symbol, SymbolTable, VariableSymbol } from "../SymbolTable"
import { asVSCRange } from "../utils";
import { Assign, AssociativeArray, Binary, Call, Expr, ExprVisitor, Glue, Grouping, Index, Invalid, List, Literal, Logical, Matrix, PostUnary, PreUnary, Variable } from "../parser/Expr";

export class Linter implements ExprVisitor<void>{
    constructor(private symbolTable: SymbolTable, private diagnostics: Diagnostic[]) {}

    public lint(expr: Expr) {
        return expr.accept(this);
    }

    visitAssignExpr(expr: Assign): void {
        throw new Error("Method not implemented.");
    }
    visitBinaryExpr(expr: Binary): void {
        expr.right.accept(this);
        expr.left.accept(this);
    }
    visitCallExpr(expr: Call): void {
        expr.callee.accept(this);
        for (const e of expr.args) {
            e.accept(this);
        }
    }
    visitGlueExpr(expr: Glue): void {
        for (const e of expr.expressions) {
            e.accept(this);
        }
    }
    visitGroupingExpr(expr: Grouping): void {
        expr.expression.accept(this);
    }
    visitIndexExpr(expr: Index): void {
        expr.expression.accept(this);
        for (const e of expr.indices) {
            e.accept(this);
        }
    }
    visitListExpr(expr: List): void {
        for (const e of expr.contents) {
            e.accept(this);
        }
    }
    visitLogicalExpr(expr: Logical): void {
        expr.left.accept(this);
        expr.right.accept(this);
    }
    visitPostUnaryExpr(expr: PostUnary): void {
        expr.expression.accept(this);
    }
    visitPreUnaryExpr(expr: PreUnary): void {
        expr.expression.accept(this);
    }

    // Terminals
    visitAssociativeArrayExpr(expr: AssociativeArray): void {
        return;
    }
    visitLiteralExpr(expr: Literal): void {
        return;
    }
    visitMatrixExpr(expr: Matrix): void {
        return;
    }
    visitVariableExpr(expr: Variable): void {
        if (expr.name.lexeme.includes('\n')) {
            this.diagnostics.push(
                new Diagnostic(asVSCRange(expr.range), "Newline in variable name. Maybe you are missing a ';' or ','.", DiagnosticSeverity.Warning)
            )
        }
        return;
    }
    visitInvalidExpr(expr: Invalid): void {
        return;
    }
}
