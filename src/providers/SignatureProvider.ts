import {
    SignatureHelp, SignatureHelpProvider, TextDocument,
    Position, CancellationToken, SignatureInformation
} from "vscode";
import { ProviderBackend } from "./ProviderBackend";
import { cleanName } from '../utils';
import { JSLFunction } from '../types';

export class JSLSignatureHelpProvider implements SignatureHelpProvider {
    constructor(private backend: ProviderBackend) {}

    public provideSignatureHelp(
        document: TextDocument, position: Position, token: CancellationToken):
        Promise<SignatureHelp> {
            
        return new Promise<SignatureHelp>((resolve, reject) => {
            const range = document.getWordRangeAtPosition(position, /(?:<<\s*)?(?:[A-Za-z_][\w\s'#$%.?\\]*:)?[A-Za-z_][\w\s'#$%.?\\]*\(/);
            const word = document.getText(range);
            const cleanedName = cleanName(word.slice(0, -1), String.prototype.toLowerCase);
            const signatureHelp = new SignatureHelp();
            
            if (cleanedName.startsWith("<<")) {
                const messageName = cleanedName.substring(2).trim();
                const message = this.backend.jslMessages[messageName];
                return resolve(this.addSignatureHelp(message, signatureHelp));
            } else if(cleanedName.includes(':')) {
                // Scoped Functions
                const [scope, func] = cleanedName.split(':');
                if (Object.prototype.hasOwnProperty.call(this.backend.jslFunctions, scope)) {
                    const obj = this.backend.jslFunctions[scope]['functions'][func];
                    return resolve(this.addSignatureHelp(obj, signatureHelp));
                }
            } else {
                // Check globals and builtins
                const builtinFunc = this.backend.jslFunctions.builtin.functions[cleanedName];
                this.addSignatureHelp(builtinFunc, signatureHelp);
                if (Object.prototype.hasOwnProperty.call(this.backend.jslFunctions, 'global')) {
                    const globalFunc = this.backend.jslFunctions.global.functions[cleanedName];
                    this.addSignatureHelp(globalFunc, signatureHelp)
                }
                return resolve(signatureHelp);
            }
            reject("No hovers found")
        })
    }

    private addSignatureHelp(func: JSLFunction, signatureHelp: SignatureHelp) {
        if (!func)
            return signatureHelp
        for(let i = 0; i < func.prototypes.length; i++) {
            const prototype = func.prototypes[i];
            const documentation = func.descriptions[i];
            const signature = new SignatureInformation(
                prototype, documentation
            );
            signatureHelp.signatures.push(signature);
        }
        return(signatureHelp)
    }
}
