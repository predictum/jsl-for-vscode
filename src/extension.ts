// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
'use strict';
import * as vscode from 'vscode';
import * as path from 'path';

import { JSLDocumentSymbolProvider } from './providers/DocumentSymbolProvider';
import { JSLSignatureHelpProvider } from './providers/SignatureProvider';
import { JSLHoverProvider } from './providers/HoverProvider';
import {
    runCurrentJMPFile, runCurrentJMPLine, runCurrentJMPSelection,
    startJMPServer, updateJSLFunctions
} from './jsl_socket';
import { readJSON } from './utils';
import { JSLScopedFunctions } from './types';
import {
    JSLCompletionProvider, LocalCompletionProvider,
    MessageCompletionProvider, ScopedNameCompletionProvider
} from './providers/CompletionProvider';
import { ProviderBackend } from './providers/ProviderBackend';
import { JSLFormattingProvider } from './providers/FormattingProvider';
import { JSLDefinitionProvider } from './providers/DefinitionProvider';
import { _parse } from './Parse';

// Use the user defined functions if they exist
let jsl_functions: JSLScopedFunctions
try {
    jsl_functions = require('./../data/JSLFunctions_User.json');
} catch (err) {
    jsl_functions = require('./../data/JSLFunctions.json');
}

const jslMessages = readJSON(path.resolve(__dirname, '../data/JSLMessages.json'));
let diagnosticCollection: vscode.DiagnosticCollection;
const backend = new ProviderBackend(jsl_functions, jslMessages); 

export function activate(ctx: vscode.ExtensionContext): void {

    diagnosticCollection = vscode.languages.createDiagnosticCollection("JSL");
    vscode.workspace.onDidOpenTextDocument((event) => reparse(event));
    vscode.workspace.onDidCloseTextDocument((event) => diagnosticCollection.delete(event.uri));
    // This can be removed if we implement parsing the entire workspace
    vscode.window.onDidChangeActiveTextEditor((event) => {
        if (event)
            reparse(event.document)
    });
    // Timer so that there is a delay between doc changes and parsing
    let timer: NodeJS.Timeout;
    vscode.workspace.onDidChangeTextDocument((event) => {
        if (timer != null) {
            clearTimeout(timer);
        }
        timer = setTimeout(() => {
            reparse(event.document)
        }, 400);
    });

    ctx.subscriptions.push(vscode.languages.registerDocumentSymbolProvider(
        {language: "jsl"}, new JSLDocumentSymbolProvider(backend)
    ));
    ctx.subscriptions.push(vscode.languages.registerSignatureHelpProvider(
        {language: "jsl"}, new JSLSignatureHelpProvider(backend), '('
    ));
    ctx.subscriptions.push(vscode.languages.registerHoverProvider(
        {language: 'jsl'}, new JSLHoverProvider(backend)
    ));
    ctx.subscriptions.push(vscode.languages.registerCompletionItemProvider(
        {language: "jsl"}, new MessageCompletionProvider(backend),
    ));
    ctx.subscriptions.push(vscode.languages.registerCompletionItemProvider(
        {language: "jsl"}, new JSLCompletionProvider(backend)
    ));
    ctx.subscriptions.push(vscode.languages.registerCompletionItemProvider(
        {language: "jsl"}, new LocalCompletionProvider(backend)
    ));
    ctx.subscriptions.push(vscode.languages.registerCompletionItemProvider(
        {language: "jsl"}, new ScopedNameCompletionProvider(backend), ':'
    ));
    ctx.subscriptions.push(vscode.languages.registerDocumentFormattingEditProvider(
        {language: 'jsl'}, new JSLFormattingProvider()
    ));
    ctx.subscriptions.push(vscode.languages.registerDefinitionProvider(
        {language: "jsl"}, new JSLDefinitionProvider(backend)
    ));

    registerCommands(ctx);
}

// TODO: Providers finish before reparse happens
function reparse(doc: vscode.TextDocument) {
    if (doc.languageId !== 'jsl') return;
    const diagnostics: vscode.Diagnostic[] = [];
    backend.parse(doc.getText(), diagnostics).then(() => {
        diagnosticCollection.set(doc.uri, diagnostics);
    });
}

const registerCommands = (context: vscode.ExtensionContext) => {
    const commands:{[key: string]: any} = {
        'jsl.startJMPServer': () => startJMPServer(backend),
        'jsl.updateFunctions': () => updateJSLFunctions(backend),
        'jsl.RunCurrentFile': runCurrentJMPFile,
        'jsl.RunCurrentSelection': runCurrentJMPSelection,
        'jsl.RunCurrentLine': runCurrentJMPLine
    };

    Object.keys(commands).forEach(cmd => {
        context.subscriptions.push(vscode.commands.registerCommand(cmd, commands[cmd]));
    });
};

export function deactivate(): void {
    diagnosticCollection.dispose();
}
