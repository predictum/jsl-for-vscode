import * as vscode from "vscode";
import * as path from 'path';
import * as fs from 'fs';
import { ScopedSymbol } from "./SymbolTable";
import { Range } from "./types";

/**
 * Wraps a string across multiple lines
 * @param {string} str - The string
 * @param {number} width - Characters per line
 */ 
export const wrapText = (str: string, width: number): string => str.replace(
    new RegExp(`(?![^\\n]{1,${width}}$)([^\\n]{1,${width}})\\s`, 'g'), '$1\n'
);

/**
 * Uppercases and removes whitespace from a string
 * @param {string} name - The string
 * @param {string} caseFunction - By default toUppercase
 */ 
export const cleanName = (name: string, caseFunction = String.prototype.toUpperCase): string => {
    return caseFunction.apply(name).replace(/\s+/g, '');
}

/**
 * Removes quotes from a string
 * @param {string} input - The string
 */ 
export const unquote = (input: string): string => {
    if (input[0] === '"' && input[input.length - 1] === '"') {
        return input.slice(1, input.length - 1);
    }
    return input;
};

/**
 * Formats a function's name based on the user's preferences.
 * @param {string} funcName - The function's unformatted name.
 */ 
export const formatName = (funcName: string): string => {
    const jslConfig = vscode.workspace.getConfiguration('jsl')
    const casing = jslConfig.get('casingOfOperators');
    const spacesInOperators = jslConfig.get('spacesInOperators');
    switch (casing) {
        case "camel Case": {
            const scopeIndex = funcName.indexOf(':');
            if (scopeIndex > -1) {
                // Leave the namespace alone
                funcName = funcName.slice(0, scopeIndex + 1) + camelize(funcName.slice(scopeIndex + 1));
            } else {
                funcName = camelize(funcName)
            }
            break;
        }
        case "lower case":
            funcName = funcName.toLowerCase();
            break;
        default:
            break;
    }
    if (!spacesInOperators) {
        funcName = funcName.replace(/\s+/g, '')
    }
    return funcName;
}

/**
 * Turns a space separated string into camelCase.
 * @param {string} str - The string.
 */ 
const camelize = (str: string) => {
    return str.replace(/(?:^\w\s+|[A-Z]|\s+\w)/g, (match, index) => {
        if (+match === 0) return "";
        return index === 0 ? match.toLowerCase() : match.toUpperCase();
    });
}

/**
 * Reads in a JSON file
 * @param {string} path - The path with the JSON.
 */ 
export const readJSON = (path: string) => {
    let raw = fs.readFileSync(path, "utf-8");
    // Remove BOM character
    if (raw.charCodeAt(0) === 0xFEFF) {
        raw = raw.substr(1);
    }
    return(JSON.parse(raw));
}

/*
 * Waits timeout milliseconds for the filePath to exist
 * See https://stackoverflow.com/questions/26165725/nodejs-check-file-exists-if-not-wait-till-it-exist
 * @param {string} filePath - The file path to check
 * @param {string} timeout - Time to wait
 */ 
export function waitForFileCreation(filePath: string, timeout: number): Promise<void> {
    return new Promise<void>(function (resolve, reject) {

        const timer = setTimeout(function() {
            watcher.close();
            reject(new Error(`File '${filePath}' was not created.`));
        }, timeout);

        fs.access(filePath, fs.constants.R_OK, function(err) {
            if (!err) {
                clearTimeout(timer);
                watcher.close();
                resolve();
            }
        });

        const dir = path.dirname(filePath);
        const basename = path.basename(filePath);
        const watcher = fs.watch(dir, function (eventType, filename) {
            if (eventType === 'rename' && filename === basename) {
                clearTimeout(timer);
                watcher.close();
                resolve();
            }
        });
    });
}

export function positionInScope(position: vscode.Position, scope: ScopedSymbol): boolean {
    if (!scope.context)
        return false
    // Check character column when in the same line
    if (scope.context.range.startLine-1 === position.line) {
        if (scope.context.range.startCol > position.character)
            return false
    }
    if (scope.context.range.endLine-1 === position.line) {
        if (scope.context.range.endCol-1 < position.character)
            return false
    }

    if (scope.context.range.startLine-1 > position.line || scope.context.range.endLine-1 < position.line)
        return false

    return true
}

export function concat_descriptions(prototypes: string[], descriptions: string[]) {
    const mdString = new vscode.MarkdownString();
    for(let i = 0; i < prototypes.length; i++) {
        mdString.appendCodeblock(prototypes[i], 'jsl');
        mdString.appendMarkdown(descriptions[i]);
    }
    return mdString;
}

// Converts parser's Ranges to a VSCode Range
export function asVSCRange(range: Range) {
    return new vscode.Range(range.startLine-1, range.startCol-1, range.endLine-1, range.endCol-1);
}
