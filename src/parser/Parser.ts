import * as vscode from 'vscode';
import { Token, TokenType } from "./Token";
import {
    Expr,
    Binary,
    PreUnary,
    Literal,
    Grouping,
    Variable,
    Logical,
    PostUnary,
    LiteralNumeric,
    LiteralString,
    List,
    Matrix,
    AssociativeArray,
    Call,
    Index,
    Glue,
    Invalid
} from './Expr';

export class ParseError extends Error {
    constructor(public token: Token, public message: string) {
        super(message)
    }
}

export class Parser {
    private tokens: Token[];
    private current = 0;
    public parseError?: ParseError;
    public diagnostics: vscode.Diagnostic[] = [];
    
    constructor(tokens: Token[]) {
        this.tokens = tokens;
    }
    
    parse() {
        try {
            const toplevel: Expr[] = [];
            toplevel.push(this.expression());
            let hasAdvanced = true;
            while (!this.isAtEnd()) {
                if (!hasAdvanced) {
                    this.advance(); // Keep moving through tokens when stuck
                } else {
                    const errorToken = this.peek();
                    let msg: string;
                    if (errorToken.type === TokenType.CLOSE_PAREN ||
                        errorToken.type === TokenType.CLOSE_BRACE ||
                        errorToken.type === TokenType.CLOSE_BRACKET) {
                        msg = `Extra '${errorToken.lexeme}' found.`;
                    } else {
                        msg = `Unexpected token '${errorToken.lexeme}'. Maybe you are missing a ';'.`;
                    }
                    this.error(errorToken, msg);
                }

                const prev = this.current;
                toplevel.push(this.expression());
                hasAdvanced = prev !== this.current;
            }
            const lastPos = toplevel[toplevel.length-1].range;
            const pos = createRange(0,0,lastPos.endLine,lastPos.endCol)
            // Recovery heuristic - Glue together partially parsed top level expressions
            // If there were no syntax errors, there should only be one expression in the list
            return toplevel.length === 1 ? toplevel[0] : new Glue(toplevel, pos);
        } catch (e) {
            return new Glue([], createRange(0,0,0,0)); // Empty script
        }
    }

    // Productions are ordered from low to high precedence
    private expression() {
        return this.glue();
    }

    private glue() {
        const expressions: Expr[] = [];
        // If below while loop breaks without finding an expression then an error ocurred
        let maybeError = false;
        do {
            // Consume GLUE
            while (this.match(TokenType.SEMICOLON)) continue;
            // Break on ending characters
            if (this.check(TokenType.CLOSE_BRACKET) ||
                    this.check(TokenType.CLOSE_BRACE) ||
                    this.check(TokenType.CLOSE_PAREN)   ||
                    this.check(TokenType.COMMA)   ||
                    this.isAtEnd()) {
                maybeError = true;
                break;
            }
            expressions.push(this.assignment());
        } while (this.match(TokenType.SEMICOLON))

        if (expressions.length > 1) {
            const pos = rangeFromExprs(expressions[0], expressions[expressions.length-1]);
            return new Glue(expressions, pos);
        } else if (expressions.length) {
            return expressions[0];
        } else {
            if (maybeError) {
                this.error(this.peek(), `Expected an expression after '${this.previous().lexeme}'.`);
                return new Invalid(this.peek().range);
            }
            // Empty expression
            return new Glue([], this.peek().range);
        }
    }

    private assignment(): Expr {
        const expr = this.logical();

        if (this.match(TokenType.ASSIGN,
                        TokenType.ADD_TO,
                        TokenType.SUBTRACT_TO,
                        TokenType.MUL_TO,
                        TokenType.DIV_TO,
                        TokenType.CONCAT_TO,
                        TokenType.VCONCAT_TO)) {
            const operator = this.previous();
            const value = this.assignment();

            const pos = rangeFromExprs(expr, value);
            return new Binary(expr, operator, value, pos);
        }

        return expr;
    }

    private logical(): Expr {
        let expr = this.comparison();
        
        while (this.match(TokenType.AND, TokenType.OR)) {
            const operator = this.previous();
            const right = this.comparison();
            const pos = rangeFromExprs(expr, right);
            // Logical, not Binary since short-circuiting can apply
            expr = new Logical(expr, operator, right, pos);
        }
        
        return expr;
    }

    private comparison(): Expr {
        let expr = this.miscBinary();
        
        while (this.match(TokenType.NOT_EQUAL,
                            TokenType.EQUAL,
                            TokenType.GREATER,
                            TokenType.GREATER_EQUAL,
                            TokenType.LESS,
                            TokenType.LESS_EQUAL)) {
            const operator = this.previous();
            const right = this.miscBinary();
            const pos = rangeFromExprs(expr, right);
            expr = new Binary(expr, operator, right, pos);
        }
        
        return expr;
    }

    // ||, |/, ::, :, <<
    private miscBinary(): Expr {
        // SEND Doesn't need a left
        if (this.match(TokenType.SEND)) {
            const operator = this.previous();
            const right = this.miscBinary();
            const pos = rangeFromExprs(operator, right);
            return new PreUnary(operator, right, pos);
        }
        let expr = this.term();
        while (this.match(TokenType.CONCAT, TokenType.VCONCAT, TokenType.DOUBLE_COLON,
                          TokenType.COLON, TokenType.SEND)) {
            const operator = this.previous();
            const right = this.term();
            const pos = rangeFromExprs(expr, right);
            expr = new Binary(expr, operator, right, pos);
        }
        return expr;
    }

    private term(): Expr {
        let expr = this.factor();
        
        while (this.match(TokenType.MINUS, TokenType.PLUS)) {
            const operator = this.previous();
            const right = this.factor();
            const pos = rangeFromExprs(expr, right);
            expr = new Binary(expr, operator, right, pos);
        }
        return expr;
    }
    
    private factor(): Expr {
        let expr = this.preUnary();
        
        while (this.match(TokenType.MUL, TokenType.EMUL,
                            TokenType.DIV, TokenType.EDIV)) {
            const operator = this.previous();
            const right = this.preUnary();
            const pos = rangeFromExprs(expr, right);
            expr = new Binary(expr, operator, right, pos);
        }
        return expr;
    }

    private preUnary(): Expr {
        if (this.match(TokenType.NOT, TokenType.MINUS)) {
            const operator = this.previous();
            const right = this.preUnary();
            const pos = rangeFromExprs(operator, right);
            return new PreUnary(operator, right, pos);
        }
        return this.power();
    }

    // Right-associative. Also includes >> and >?.
    private power(): Expr {
        const expr = this.postUnary();
        
        if (this.match(TokenType.POWER, TokenType.PAT_IMMEDIATE, TokenType.PAT_CONDITIONAL)) {
            const operator = this.previous();
            const right = this.preUnary();
            const pos = rangeFromExprs(expr, right);
            return new Binary(expr, operator, right, pos);
        }
        return expr;
    }

    private postUnary(): Expr {
        let expr = this.callOrIndex();
        while (this.match(TokenType.INC, TokenType.DEC, TokenType.BACK_QUOTE)) {
            const operator = this.previous();
            const pos = rangeFromExprs(expr, operator);
            expr = new PostUnary(expr, operator, pos);
        }
        return expr;
    }

    private callOrIndex(): Expr {
        let expr = this.scopedUnary();

        while (true) { 
            if (this.match(TokenType.OPEN_PAREN)) {
                expr = this.finishCall(expr);
            } else if(this.match(TokenType.OPEN_BRACKET)) {
                expr = this.finishIndex(expr);
            } else {
                break;
            }
        }
        return expr;
    }

    private scopedUnary(): Expr {
        if (this.match(TokenType.COLON, TokenType.DOUBLE_COLON, TokenType.TRIPLE_COLON)) {
            const operator = this.previous();
            const right = this.scopedBinary();
            const pos = rangeFromExprs(operator, right);
            return new PreUnary(operator, right, pos);
        }
        return this.scopedBinary();
    }

    private scopedBinary(): Expr {
        let expr = this.primary();
        while (this.match(TokenType.COLON)) {
            const operator = this.previous();
            const right = this.primary();
            if (expr instanceof LiteralNumeric || right instanceof LiteralNumeric) {
                this.error(operator, "The ':' operator does not accept numeric values.")
            }
            const pos = rangeFromExprs(expr, right);
            expr = new Binary(expr, operator, right, pos);
        }
        return expr;
    }

    private primary(): Expr {
        if (this.match(TokenType.NUMBER)) {
            return new LiteralNumeric(this.previous().literal, this.previous().range);
        }

        if (this.match(TokenType.STRING)) {
            return new LiteralString(this.previous().literal, this.previous().range);
        }
        
        if (this.match(TokenType.NAME)) {
            return new Variable(this.previous(), this.previous().range);
        }
        
        if (this.match(TokenType.OPEN_BRACE)) {
            const start = this.previous();
            const list = this.list();
            const pos = rangeFromExprs(start, this.previous()) 
            return new List(list, pos);
        }

        if (this.match(TokenType.OPEN_BRACKET)) {
            const start = this.previous();
            // lookahead two for arrow operator
            if (!this.check(TokenType.CLOSE_BRACKET) && 
                (this.check(TokenType.ARROW) || 
                this.lookahead(1).type === TokenType.ARROW || 
                this.lookahead(2).type === TokenType.ARROW)) {
                const aa = this.associativeArray();
                const pos = rangeFromExprs(start, this.previous());
                return new AssociativeArray(aa, pos);
            } else {
                const mat = this.matrix();
                const pos = rangeFromExprs(start, this.previous());
                return new Matrix(mat, pos);
            }
        }

        if (this.match(TokenType.OPEN_PAREN)) {
            return this.finishGroup();
        }

        let msg = `Unexpected '${this.peek().lexeme}'.`;
        let start = {startLine: 0, startCol: 0, endLine: 0, endCol: 0};
        if (this.current !== 0) {
            msg = `Expected an expression after '${this.previous().lexeme}'.`;
            start = this.previous().range;
        }
        this.error(this.peek(), msg);
        const end = this.peek().range;
        return new Invalid(createRange(start.endLine, start.endCol, end.startLine, end.startCol));
    }

    private finishCall(callee: Expr): Expr {
        const args: Expr[] = [];
        const call = this.previous(2);
        let callName = "call";
        if (call.type === TokenType.NAME) {
            callName = `'${call.lexeme}' `;
        }
        const callStart = this.previous().range;
        while(this.match(TokenType.COMMA)) continue; // allow multiple commas at start
        
        let callComplete = false;
        let hasAdvanced = true;
        while (!this.isAtEnd() && !callComplete) { // Loop for continuous error recovery of function arguments
            if (!hasAdvanced) {
                this.advance(); // Keep moving through tokens when stuck
            }
            const prev = this.current;
            if (!this.check(TokenType.CLOSE_PAREN)) {
                do {
                    while(this.match(TokenType.COMMA)) continue; // allow multiple commas inbetween
                    if(!this.check(TokenType.CLOSE_PAREN))
                        args.push(this.expression());
                } while (this.match(TokenType.COMMA));
            }

            if (!this.match(TokenType.CLOSE_PAREN)) {
                // Error recovery heuristic - assume missing ; or , and keep reading expressions as arugments
                let msg = `Unexpected token '${this.peek().lexeme}' when parsing arguments of ${callName} on line ${callStart.startLine}. Maybe you are missing a ';' or ','.`;
                if(this.isAtEnd()) {
                    msg = `Did not find the closing ')' when parsing the ${callName === 'call' ? '' : callName+' '}call on line ${callStart.startLine}.`
                }
                this.error(this.peek(), msg);
            } else {
                callComplete = true
            }
            hasAdvanced = prev !== this.current;
        }
        if (this.isAtEnd() && !callComplete) {
            this.error(this.peek(), "Missing a ')'.")
        }
        const pos = rangeFromExprs(callee, this.previous());
        return new Call(callee, args, pos);
    }

    private finishIndex(expr: Expr): Expr {
        const indices: Expr[] = [];
        const start = this.previous();
        let hasAdvanced = true;
        while (!this.check(TokenType.CLOSE_BRACKET) && !this.isAtEnd() && hasAdvanced) {
            const prev = this.current;
            indices.push(this.expression());
            hasAdvanced = this.current !== prev;
            if(this.match(TokenType.CLOSE_BRACKET))
                return new Index(expr, indices, rangeFromExprs(start, this.previous()));
            this.consume(TokenType.COMMA, "Expected a ',' or ']'.");
        }
        this.consume(TokenType.CLOSE_BRACKET, "Expected a ']'.");
        const pos = rangeFromExprs(start, this.previous());
        return new Index(expr, indices, pos);
    }

    private list(): Expr[] {
        const contents: Expr[] = []
        while(this.match(TokenType.COMMA)) continue; // allow multiple commas at start
        let hasAdvanced = true;
        while(!this.check(TokenType.CLOSE_BRACE) && !this.isAtEnd() && hasAdvanced) {
            const prev = this.current;
            contents.push(this.expression());
            hasAdvanced = prev !== this.current;
            if(this.match(TokenType.CLOSE_BRACE))
                return contents;
            this.consume(TokenType.COMMA, "Expected a ',' or '}'.");
            while(this.check(TokenType.COMMA)) this.advance(); // allow multiple commas inbetween
        }
        if(this.previous().type === TokenType.COMMA)
            this.error(this.previous(), "Unexpected ',' at end of list.");
        this.consume(TokenType.CLOSE_BRACE, "Expected a '}'.");
        return contents;
    }

    private matrix(): LiteralNumeric[][] {
        const contents: LiteralNumeric[][] = [];
        let rowLength;
        let row: LiteralNumeric[];
        while (!this.check(TokenType.CLOSE_BRACKET) && !this.isAtEnd()) {
            row = [];
            while(!this.check(TokenType.SEMICOLON) &&
                    !this.check(TokenType.COMMA) &&
                    !this.check(TokenType.CLOSE_BRACKET) && 
                    !this.isAtEnd()) {

                // TODO: Technically a space separated + or - is allowed on it's own
                // representing +1, -1, but...
                row.push(this.numericLiteral());
            }

            if (!rowLength) {
                rowLength = row.length;
            } else if (row.length !== rowLength) {
                this.error(this.peek(), "Inconsistent lengths of rows in matrix.");
            }
            contents.push(row);

            if (this.check(TokenType.SEMICOLON) || this.check(TokenType.COMMA)) {
                this.advance(); // Advance past the ; or ,
                if (this.match(TokenType.SEMICOLON)) {
                    this.error(this.peek(), "Extra ';' in matrix.");
                } else if (this.match(TokenType.COMMA)) {
                    this.error(this.peek(), "Extra ',' in matrix.");
                }
            }
        }
        if (this.previous().type === TokenType.SEMICOLON) {
            this.error(this.previous(), "Unexpected ';' at end of matrix.");
        } else if (this.previous().type === TokenType.COMMA) {
            this.error(this.previous(), "Unexpected ',' at end of matrix.");
        }
        this.consume(TokenType.CLOSE_BRACKET, "Expected a ']'.");

        return contents;
    }

    private associativeArray(): Map<Literal, Expr> {
        const contents = new Map<Literal, Expr>();
        if (this.check(TokenType.ARROW) && this.lookahead(1).type === TokenType.CLOSE_BRACKET) {
            this.advance();
            this.advance();
            return contents;
        }
        while(!this.check(TokenType.CLOSE_BRACKET) && !this.isAtEnd()) {
            const [k, v] = this.keyValue();
            contents.set(k, v);
            if (!this.check(TokenType.CLOSE_BRACKET))
                this.consume(TokenType.COMMA, "expected a ',' or ']'.");
        }
        if(this.previous().type === TokenType.COMMA)
            this.error(this.previous(), "Unexpected ',' at end of associative array literal.");
        this.consume(TokenType.CLOSE_BRACKET, "expected a ']'.");
        return contents;
    }

    private keyValue(): [Literal, Expr] {
        let key: Literal;
        if (this.check(TokenType.PLUS) || this.check(TokenType.MINUS)) {
            key = this.numericLiteral();
        } else if(this.match(TokenType.STRING, TokenType.NUMBER)) {
            key = new Literal(this.previous().literal, this.previous().range);
        } else if (this.check(TokenType.ARROW)) {
            // TODO: Defaults aren't allowed everywhere.
            key = new Literal(undefined, this.previous().range);
        } else {
            this.error(this.peek(), "Invalid token in associative array. Literal associative arrays ([=>]) can only have strings or numbers as keys.");
            // Recover and keep going
            this.advance();
            key = new Literal("__ERROR__", this.previous().range);
        }
        this.consume(TokenType.ARROW, "expected a '=>'.");
        const val = this.expression();
        return [key, val]
    }

    private numericLiteral(): LiteralNumeric {
        let sign = 1;
        if (this.match(TokenType.MINUS)) {
            sign = -1;
        } else if (this.match(TokenType.PLUS)) {
            sign = 1;
        }
        if (!this.check(TokenType.NUMBER)) {
            this.error(this.peek(), "Invalid matrix token. Only numeric literals can be used.");
        }
        const numToken = this.advance();
        let num = NaN;
        if (numToken.type === TokenType.NUMBER) {
            num = numToken.literal as number;
        }
        return new LiteralNumeric(sign * (num as number), this.previous().range);
    }

    private match(...types: TokenType[]): boolean {
        for (const type of types) {
            if (this.check(type)) {
                this.advance();
                return true;
            }
        }
        return false;
    }

    // A Group should only contain a single expression()
    // But as error recovery, if the closing ')' is not found at the end of the
    // expression this method will assume it is missing a semi-colon and keep parsing
    private finishGroup() {
        const groupExprs: Expr[] = [];
        const start = this.previous();
        if (this.match(TokenType.CLOSE_PAREN)) {
            this.error(this.previous(), "Empty '()'.");
            const range = rangeFromExprs(this.previous(2), this.previous());
            return new Invalid(range);
        }
        groupExprs.push(this.expression());
        if (this.match(TokenType.CLOSE_PAREN)) {
            return groupExprs[0]; // No syntax error
        }

        // Error recovery path. Assume a missing semi-colon and keep parsing the expression.
        // This is better than assuming that the ')' is missing because in the case of a
        // missing ')' the parser would usually end up parsing to the end of the file anyway
        let hasAdvanced = true;
        do {
            const errorToken = this.peek();
            // Exit loop if other type of closing bracket or end of file
            if (errorToken.type === TokenType.CLOSE_BRACE ||
                errorToken.type === TokenType.CLOSE_BRACKET ||
                errorToken.type === TokenType.COMMA ||
                this.isAtEnd()) {
                let msg: string;
                if (this.isAtEnd()) {
                    msg = "Missing a ')'.";
                } else if (errorToken.type === TokenType.COMMA) {
                    msg = "Unexpected ',' in grouped expression between parentheses.";
                } else {
                    msg = `Mismatched brackets. Expected ')' but found '${errorToken.lexeme}'.`
                }
                this.error(errorToken, msg);
                const pos = rangeFromExprs(groupExprs[0], groupExprs[groupExprs.length-1]);
                const expr = groupExprs.length === 1 ? groupExprs[0] : new Glue(groupExprs, pos);
                return new Grouping(expr, rangeFromExprs(start, this.previous()));
            }

            const msg = `Unexpected token '${errorToken.lexeme}'. Maybe you are missing a ';'.`;
            this.error(errorToken, msg);

            const prev = this.current;
            groupExprs.push(this.expression());
            hasAdvanced = prev !== this.current;

        } while (!this.match(TokenType.CLOSE_PAREN) && hasAdvanced);
    
        const pos = rangeFromExprs(groupExprs[0], groupExprs[groupExprs.length-1]);
        const expr = groupExprs.length === 1 ? groupExprs[0] : new Glue(groupExprs, pos);
        return new Grouping(expr, rangeFromExprs(start, this.previous()));
    }

    private consume(type: TokenType, message: string) {
        if (this.check(type))
            return this.advance();

        this.error(this.peek(), message);
    }
    
    private addDiagnostic(token: Token, message: string) {

        const range = new vscode.Range(token.range.startLine-1, token.range.startCol-1,
            token.range.endLine-1, token.range.endCol-1);
        const diagnostic = new vscode.Diagnostic(range, message, vscode.DiagnosticSeverity.Error);

        this.diagnostics.push(diagnostic);
    }

    private error(token: Token, message: string) {
        if (token.hadError) return; // No need to report possible double errors

        token.hadError = true;
        this.addDiagnostic(token, message);
        this.parseError = new ParseError(token, message);

        return this.parseError;
    }
    
    private check(type: TokenType): boolean {
        if (this.isAtEnd()) return false;
        return this.peek().type === type;
    }
    
    private advance(): Token {
        if (!this.isAtEnd()) this.current++;
        return this.previous();
    }
    
    private isAtEnd(): boolean {
        return this.peek().type === TokenType.EOF;
    }
    
    private peek(): Token {
        return this.tokens[this.current];
    }

    // Lookahead. Should not use to check EOF.
    private lookahead(n: number): Token {
        if (this.current + n >= this.tokens.length)
            return this.tokens[this.tokens.length]; // EOF
        return this.tokens[this.current + n];
    }
    
    private previous(n=1): Token {
        return this.tokens[this.current-n];
    }
}

function createRange(startLine: number, startCol: number, endLine: number, endCol: number) {
    return {startLine: startLine, startCol: startCol, endLine: endLine, endCol: endCol}
}

function rangeFromExprs(first: Expr | Token, last: Expr | Token) {
    const firstPos = first.range;
    const lastPos = last.range;
    return createRange(firstPos.startLine,firstPos.startCol,lastPos.endLine,lastPos.endCol);
}