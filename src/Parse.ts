import { Diagnostic } from "vscode";
import { Expr } from "./parser/Expr";
import { Lexer } from "./parser/Lexer";
import { Parser } from "./parser/Parser";

export function _parse(source: string, diagnostics?: Diagnostic[]): Thenable<Expr> {
    return new Promise<Expr>((resolve, _reject) => {

        const lexer = new Lexer(source);
        const tokens = lexer.scanTokens();
        const parser = new Parser(tokens);
        const tree = parser.parse();

        // Modify the passed diagnostics list
        diagnostics?.push(...lexer.diagnostics.concat(parser.diagnostics));
        resolve(tree)
    });
}
