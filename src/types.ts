export interface JSLScopedFunctions {
    [x: string]: ScopedFunctions
}

export interface JSLFunctions {
    [x: string]: JSLFunction
}

interface ScopedFunctions {
    functions: Record<string, JSLFunction>,
    unformatted_name: string
}

export interface JSLFunction {
    args: string[][],
    descriptions: string[],
    prototypes: string[],
    unformatted_name: string
}

export interface Range {
    startLine: number,
    startCol: number,
    endLine: number,
    endCol: number
}