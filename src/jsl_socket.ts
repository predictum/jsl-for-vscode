import * as vscode from 'vscode';
import * as path from 'path';
import * as os from 'os'
import * as fs from 'fs';
import { Socket } from 'net';
import { exec } from 'child_process';
import { ProviderBackend } from './providers/ProviderBackend';

const output = vscode.window.createOutputChannel("JSL");
// The port number and hostname of the server.
const port = 14141;
const host = 'localhost';

export async function startJMPServer(backend: ProviderBackend): Promise<void> {
    const started = await _startJMPServer();
    if (started) updateJSLFunctions(backend, false);
}

async function _startJMPServer(): Promise<boolean> {
    const scriptPath = path.resolve(__dirname, "../jsl/StartServer.jsl");
    const jmpLocation = await getJMPLocation();
    if (!jmpLocation)
        return false

    const jmpRunning = await isRunning(jmpLocation);
    if (jmpRunning) {
        openScript(jmpLocation, scriptPath).catch((err: Error) => {
            vscode.window.showErrorMessage("Error while opening StartServer.jsl:\n" + err.message, {modal: true})
        });
    } else {
        // First open JMP then open StartServer with a timeout so custom functions can load in
        openJMP(jmpLocation).then(() => {
            setTimeout(function () {
                openScript(jmpLocation, scriptPath);
            }, 1000);
        }
        ).catch((err: Error) => {
            vscode.window.showErrorMessage("Error while starting JMP server:\n" + err.message, {modal: true})
        });
    }
    return true
}

export async function updateJSLFunctions(backend: ProviderBackend, tryStart = true): Promise<void> {
    const scriptPath = path.resolve(__dirname, "../jsl/UpdateFunctionsList.jsl");
    const command = `include("${scriptPath}", << newContext)`;
    await runJMPCommand(command, true, tryStart);
    backend.updateProviders();
}

export function runCurrentJMPFile(): void {
    const editor = vscode.window.activeTextEditor!;
    const document = editor.document;
    runJMPCommand(document.getText());
}

export function runCurrentJMPSelection(): void {
    const editor = vscode.window.activeTextEditor!;
    const document = editor.document;
    const txt = document.getText(editor.selection);

    if(txt !== ""){
        runJMPCommand(txt);
    } else {
        runCurrentJMPFile();
    }
}

export function runCurrentJMPLine(): void {
    const editor = vscode.window.activeTextEditor!;
    const document = editor.document;
    let txt = document.getText(editor.selection);

    if(txt === ""){
        txt = document.lineAt(editor.selection.start.line).text;
    }
    runJMPCommand(txt);
}

export async function runJMPCommand(command: string, mute = false, tryStart = true): Promise<string> {
    const editor = vscode.window.activeTextEditor!;
    const document = editor.document;
    const filepath = document.uri.fsPath;
    const dir = path.dirname(filepath);

    if (!mute) {
        output.show(true);
        output.appendLine("\n//:*/");
        output.appendLine(command);
        output.appendLine("/*:");
    }
    command = 'setdefaultdirectory("DV_DIR");\n'.replace("DV_DIR", dir) + command;

    return new Promise((resolve, reject) => {
        tryConnection(1000, 30, tryStart)
        .then((client) => {
            client.on('data', function(chunk: Record<string, unknown>) {
                const result = chunk.toString().replace(/\u2028+/g, '\n');
                if (!mute) {
                    output.appendLine("");
                    output.appendLine(result);
                }
                client.destroy();
                resolve(result);
            });
            client.write(command);
        })
        .catch(
            (err) => {
                if (tryStart){
                    vscode.window.showErrorMessage(err.message);
                    output.appendLine("\nERROR: Failed to execute.");
                    reject(err)
                }
                // If we are not trying to start the server, then we don't want to give the window error (it's a lot of notifications)
                else{
                    output.appendLine("\n"+err.message);
                    reject(err)
                }
            }
        )
    });
    
}

// Utility function to wait specified time before .then()
const wait = (ms: number) => new Promise(r => setTimeout(r, ms));

function tryConnection(delay: number, retries: number, tryStart = true, started = false): Promise<Socket>{
    return new Promise<Socket>((resolve, reject) => {
        return openConnection()
            .then(resolve)
            .catch(async (err: NodeJS.ErrnoException) => {
                if (err.code === "ECONNREFUSED" && retries > 0) {
                    let hasLocation = true;
                    if (!started) {
                        if(tryStart){
                            hasLocation = await _startJMPServer();
                        } else { // If we are not trying to start the server, then we don't want to give the window error (it's a lot of notifications)
                            reject(new Error("JMP server not running. Please start the server and try again."));
                        }
                    }
                    if (hasLocation) {
                        // Repeat with delay until success or too many retries
                        return wait(delay)
                        .then(tryConnection.bind(null, delay, retries - 1, true, true))
                        .then(resolve)
                        .catch(reject);
                    }
                }
                return reject(err);
            });
    });
}

function openConnection() {
    return new Promise<Socket>((resolve, reject) => {
        const client = new Socket();
        client.connect({ port: port, host: host });
        client.on('connect', () => resolve(client));
        client.on('error', reject);
    });
}

// https://stackoverflow.com/a/58844917
async function isRunning(processName: string): Promise<boolean> {
    const cmd = (() => {
        switch (process.platform) {
        case 'win32': {
            // No way to find path of jmp process on windows
            // Accept any open jmp instance
            processName = "jmp.exe";
            return `tasklist /fi "IMAGENAME eq jmp.exe"`;
        }
        case 'darwin': {
            return `ps -ax | grep "${processName}" | grep -v grep || [[ $? == 1 ]]`;
        }
        default: return ""
        }
    })()

    return new Promise((resolve, reject) => {
        exec(cmd, (err: Error | null, stdout: string, stderr: string) => {
        if (err)
            reject(err);

        resolve(stdout.toLowerCase().indexOf(processName.toLowerCase()) > -1)
        })
    })
}

async function openJMP(jmpLocation: string): Promise<boolean> {
    const cmd = (() => {
        switch (process.platform) {
            case 'win32': return `Start-Process "${jmpLocation}"`
            case 'darwin': return `open "${jmpLocation}" -g`
            default: return ""
        }
    })()

    let shell: string;
    switch (process.platform) {
        case 'win32': shell = "powershell"; break;
        case 'darwin': shell = "/bin/sh"; break;
        default: ""
    }
    return new Promise((resolve, reject) => {
        exec(cmd, {shell: shell}, (err: Error | null, stdout: string, stderr: string) => {
            if (err) reject(err);
            resolve(true);
        });
    })
}

async function openScript(jmpLocation: string, scriptPath: string): Promise<boolean> {
    const cmd = (() => {
        switch (process.platform) {
            case 'win32': return `Invoke-Item "${scriptPath}"`
            case 'darwin': return `open "${scriptPath}" -a "${jmpLocation}" -g`
            default: return ""
        }
    })()

    let shell: string;
    switch (process.platform) {
        case 'win32': shell = "powershell"; break;
        case 'darwin': shell = "/bin/sh"; break;
        default: ""
    }
    return new Promise((resolve, reject) => {
        exec(cmd, {shell: shell}, (err: Error | null, stdout: string, stderr: string) => {
            if (err) reject(err);
            resolve(true)
        });
    })
}

async function getJMPLocation() {
    const jslConfig = vscode.workspace.getConfiguration('jsl')
    let jmpLocation: string | undefined = jslConfig.get('JMPLocation');
    if (!jmpLocation) {
        for (const p of getSearchPaths()) {
            if (fs.existsSync(p)) {
                jmpLocation = p;
                break;
            }
        }
    }
    if (!jmpLocation) {
        // Prompt for the path
        jmpLocation = await vscode.window.showInputBox({prompt: "Set the path to your JMP executable."})
        if (jmpLocation) {
            jslConfig.update('JMPLocation', jmpLocation);
        } else {
            vscode.window.showInformationMessage("You can set the path to your JMP executable in VSCode's settings")
        }
    }
    return jmpLocation;
}

function getSearchPaths(): string[] {
    const homedir = os.homedir()
    const pathsToSearch: string[] = []
    const earliestVersion = 13;
    const lastSasVersion = 17;
    const latestVersion = 18;
    if (process.platform === 'win32') {
        const programFiles = process.env.PROGRAMFILES ? process.env.PROGRAMFILES : "";
        for (let v = latestVersion; v > lastSasVersion; v--) {
            pathsToSearch.push(path.join(homedir, 'Program Files', 'JMP', 'JMPPRO', v.toString(), 'jmp.exe'));
            pathsToSearch.push(path.join(programFiles, 'JMP', 'JMPPRO', v.toString(), 'jmp.exe'));
            pathsToSearch.push(path.join(homedir, 'Program Files', 'JMP', 'JMP', v.toString(), 'jmp.exe'));
            pathsToSearch.push(path.join(programFiles, 'JMP', 'JMP', v.toString(), 'jmp.exe'));
        }
        for (let v = lastSasVersion; v >= earliestVersion; v--) {
            pathsToSearch.push(path.join(homedir, 'Program Files', 'SAS', 'JMPPRO', v.toString(), 'jmp.exe'));
            pathsToSearch.push(path.join(programFiles, 'SAS', 'JMPPRO', v.toString(), 'jmp.exe'));
            pathsToSearch.push(path.join(homedir, 'Program Files', 'SAS', 'JMP', v.toString(), 'jmp.exe'));
            pathsToSearch.push(path.join(programFiles, 'SAS', 'JMP', v.toString(), 'jmp.exe'));
        }
    }
    else if (process.platform === 'darwin') {
        for (let v = latestVersion; v >= earliestVersion; v--) {
            pathsToSearch.push(path.join(homedir, 'Applications', 'JMP Pro ' + v.toString() + '.app'));
            pathsToSearch.push(path.join('/', 'Applications', 'JMP Pro ' + v.toString() + '.app'));
            pathsToSearch.push(path.join(homedir, 'Applications', 'JMP ' + v.toString() + '.app'));
            pathsToSearch.push(path.join('/', 'Applications', 'JMP ' + v.toString() + '.app'));
        }
    }
    return pathsToSearch
}
