import * as chai from 'chai';

// You can import and use all API from the 'vscode' module
// as well as import your extension to test it
import * as vscode from 'vscode';
// import * as myExtension from '../../extension';

async function newDoc(content: string) {
    const doc = await vscode.workspace.openTextDocument({
        language: 'jsl'
    });
	const edit = new vscode.WorkspaceEdit();
	edit.insert(doc.uri, new vscode.Position(0,0), content)
	await vscode.window.showTextDocument(doc);
	await vscode.workspace.applyEdit(edit);
	return doc
}

suite('Extension Test Suite', () => {
	const doc = newDoc(
`names default to here(1);
outer = 1;
foo = function({}, {DEFAULT LOCAL},
	inner = 1;
);
foo();
`);

	test('Doc symbols', async () => {
		const symbols: vscode.DocumentSymbol[] = await vscode.commands.executeCommand("vscode.executeDocumentSymbolProvider", (await doc).uri);
		chai.assert.sameDeepMembers(symbols.map(s=>s.name), ["outer", "foo"]);
		chai.assert.sameDeepMembers(symbols.map(s=>s.detail), ["here", "here"]);
		chai.assert.sameDeepMembers(symbols.map(s=>s.kind), [vscode.SymbolKind.Variable, vscode.SymbolKind.Function]);
		chai.assert.equal(symbols[1].children.length, 1);
		chai.assert.equal(symbols[1].children[0].name, "inner");
		chai.assert.equal(symbols[1].children[0].detail, "foo");
	});

	test('Completions outside foo', async () => {
		const completions: vscode.CompletionList = await vscode.commands.executeCommand(
			"vscode.executeCompletionItemProvider",
			(await doc).uri,
			new vscode.Position(5, 0)
		);
		const labels = completions.items.map(c=>c.label.toString().toLowerCase());
		chai.assert.includeMembers(labels, ['cos', 'glue'], "Builtins");
		chai.assert.includeMembers(labels, ['outer', 'foo'], "Locals");
		chai.assert.notInclude(labels, 'inner');
	});

	test('Completions inside foo', async () => {
		const completions: vscode.CompletionList = await vscode.commands.executeCommand(
			"vscode.executeCompletionItemProvider",
			(await doc).uri,
			new vscode.Position(3, 0)
		);
		const labels = completions.items.map(c=>c.label.toString().toLowerCase());
		chai.assert.includeMembers(labels, ['cos', 'glue'], "Builtins");
		chai.assert.includeMembers(labels, ['outer', 'foo', 'inner'], "Locals");
	});

	test('Hover', async () => {
		const hovers: vscode.Hover[] = await vscode.commands.executeCommand(
			"vscode.executeHoverProvider",
			(await doc).uri,
			new vscode.Position(0, 1)
		);
		chai.assert.lengthOf(hovers, 1);
		const hoverContent = hovers[0].contents[0] as vscode.MarkdownString;
		chai.assert.match(hoverContent.value, /names\s?default\s?to\s?here/i)
	});

	test('Go to definition', async () => {
		const uri = (await doc).uri;
		const locs: vscode.Location[] = await vscode.commands.executeCommand(
			"vscode.executeDefinitionProvider",
			uri,
			new vscode.Position(5, 0)
		);
		chai.assert.equal(locs.length, 1);
		chai.assert.deepEqual(locs[0].uri, uri);
		chai.assert.deepEqual(locs[0].range.start, new vscode.Position(2, 6));
	});

	suiteTeardown(() => {
		vscode.commands.executeCommand('workbench.action.closeActiveEditor');
	});
});
