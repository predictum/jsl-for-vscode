# Change Log
## 0.18.0
* Update JMP Server for JMP 18
* Add syntax highlighting for expressions in evalInsert strings (#92)
* Fix block comment highlighting for `/**` (#93)
* Fix Default Directory set incorrectly on network drives (!102)

## 0.17.0
* Add hovering for variables (#81)
* Lint variable names with newlines (!99)

## 0.15.0
* Add missing "/=" operator (#83)

## 0.14.0
* Allow non alphanumeric symbols in identifiers (#80)
* Update jmp paths to search to include JMP 17 (!89)

## 0.13.0
* Add variables from list assignments as symbols e.g. {a,b} = {1,2} (#75)
* Add message docstrings to completions, hovers, and signatures (!85)

## 0.12.0
* Incorporate hand-written parser for better error recovery (!80)

## 0.11.0
* Add empty matrix syntax to grammar (#71)
* Fix exponent rule in numbers to allow multiple digits (!78)

## 0.10.0
* Add signature help to builtin and custom functions (#66)
* Add command to update custom functions from the scripting index (#69)
* Prevent DeleteSymbols from stopping the JMP server (#73)
* Add completions for builtin and custom functions (#68)
* Add document formatting (#72)
* Add go to definition (!74)
* Syntax highlighting for all functions (#48)

## 0.7.0
* Improve syntax error messages (#52)
* Run JSL without an addin (#46)
* Fix running JSL with `Names Default to Here(0)` (#45)
* Fix JSL: Run Current File to run unsaved version of file (#47)
* Fix grammar to allow index expression as scope (#63)

## 0.6.0
* Add syntax error reporting (#49)
* Move symbol providing to ANTLR (#50)
* Improve document symbols (#53)
* Add syntax highlighting of escaped characters in strings (!53)

## 0.5.0
* #41 - Fixed nested block comments to display correctly
* Add command to run current line (Alt + Enter) (#44)
* Change default command for run current selection to Shift + Enter (#44)
* Hover text for user defined custom functions (!51)

## 0.4.0
* #42 - fixed issue with mac not being able to use the socket
* #43 - addin returning functions shouldn't fail now.  

## 0.3.0
* Relative pathing should work with includes (if the file exists)
* fixed some annoying snippet issues
* fixed some hovertext functions with spaces

## 0.2.0
* Better symbol finding for functions/methods
* Making a print statement in JMP to let the user know the server is running
* Fixed an issue with ranges not being correct

## 0.1.0

- Initial release
- Syntax Highlighting
- Snippets
- Symbol Definition