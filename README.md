## JSL VSCode Extension
[![Marketplace Version](https://vsmarketplacebadges.dev/version/vincefaller.jmp-scripting.png)](https://marketplace.visualstudio.com/items?itemName=vincefaller.jmp-scripting) [![Installs](https://vsmarketplacebadges.dev/installs/vincefaller.jmp-scripting.png)](https://marketplace.visualstudio.com/items?itemName=vincefaller.jmp-scripting) [![Downloads](https://vsmarketplacebadges.dev/downloads/vincefaller.jmp-scripting.png)](https://marketplace.visualstudio.com/items?itemName=vincefaller.jmp-scripting) [![Rating](https://vsmarketplacebadges.dev/rating/vincefaller.jmp-scripting.png)](https://marketplace.visualstudio.com/items?itemName=vincefaller.jmp-scripting)

A Visual Studio Code extension with increasing support for the JMP Scripting Language (JSL)  

Developed By [Predictum Inc](https://predictum.com/). 

### Quick Start
1. Install the **JSL: JMP Scripting** extension.
1. Open a JSL file and start scripting!


### Running JSL
To run JSL from VSCode there are three options available in the command palette:
* JSL: Run Current Line (<kbd>Alt</kbd>+<kbd>Enter</kbd>)
* JSL: Run Current Selection (<kbd>Shift</kbd>+<kbd>Enter</kbd>)
* JSL: Run Current File

The first time any of these commands runs, JMP will open and will start running a server that executes any JSL that is sent to it. If JMP is already open, the open instance will be used to run this server. A button is available in JMP to stop the server from running at any time.

Running any JSL for the first time will also update the hover text for functions with any `New Custom Function`s that are in the scripting index.

**Note**: If the extension can't find your JMP installation or if you want to specify a different version of JMP, you must specify the `jsl.JMPlocation` setting. You can use the command palette (<kbd>Ctrl</kbd>/<kbd>Cmd</kbd>+<kbd>Shift</kbd>+<kbd>P</kbd>) to search for `Open Settings (UI)` and then search for `jsl` to see the available settings. The path provided should be similar to the examples in the table below:

|   OS    |              Example Path             |
| ------- | ------------------------------------- |
| Mac     | `/Applications/JMP 16.app`            |
| Windows | `C:\Program Files\SAS\JMP\16\jmp.exe` |


### Features
* Syntax highlighting  
* Document Symbols/Outlining  
  <img src="https://gitlab.com/predictum/jsl-for-vscode/raw/master/assets/symbols.png" alt="document symbols" width="500"/>  
* Syntax error reporting  
  ![Syntax Error Highlighting](https://gitlab.com/predictum/jsl-for-vscode/raw/master/assets/syntax_error.gif)  
* Hover Help  
  ![_hover](https://gitlab.com/predictum/jsl-for-vscode/raw/master/assets/hover.gif)
* Running JSL  
  ![_running_jmp](https://gitlab.com/predictum/jsl-for-vscode/raw/master/assets/RunningJSL.gif)  
* [Snippets](#snippets)  
  ![_snippets](https://gitlab.com/predictum/jsl-for-vscode/raw/master/assets/snippets.gif)  
* Go to definition
  * Ctrl/Cmd + click a variable name to go to where the variable was defined
* Document formatting
  * This uses JMP as the formatter and requires you to be able to [run JSL](#running-jsl)
  * Formatting preferences can be controlled in JMP's preferences (See: Script Editing > JSL Formatting)

### Snippets
Currently these are existing snippets.  Snippets for functions have comments compatible for use with [Natural Docs](https://www.naturaldocs.org/)
* for
* function
* new custom function
* method

### Extension Settings
| Setting | Description | Default |
| ------- | ---------- | -------- |
| `jsl.JMPLocation` | The path to the JMP executable to use when running JSL. | empty |
| `jsl.spacesInOperators` | Include spaces in functions suggested by code completion. | No spaces |
| `jsl.casingOfOperators` | Specify the casing of functions suggested by code completion. | camel Case |

### List of JSL Commands
| Command | Description | Default Key Binding |
| --- | --- | --- |
| `JSL: Update Functions from Scripting Index` | Update VSCode with any custom functions you have added to the scripting index. | |
| `JSL: Start JMP Server` | Starts the JMP server without running any JSL. | |
| `JSL: Run Current Line` | Runs the text on the same line as the cursor. If text is selected, runs the selected text instead. | <kbd>Alt</kbd>+<kbd>Enter</kbd> |
| `JSL: Run Current Selection` | Runs the selected text. If no text is selected, runs the whole file. | <kbd>Shift</kbd>+<kbd>Enter</kbd> |
| `JSL: Run Current File`| Runs the whole file. | |


### Issues
Please [visit the repo](https://gitlab.com/predictum/jsl-for-vscode) to put in additional issues or feature requests. 

### Contact
Feel free to email me bpeachey@predictum.com

